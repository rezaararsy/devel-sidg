<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="description" content="Sistem Informasi Dokter Gigi merupakan website yang digunakan untuk membantuk dokter gigi dalam melakukan perawatan terhadap pasien.">
  <title>SIDG</title>

  <!--CSS-->
  <link href="<?php echo base_url("asset/css/bootstrap.css");?>" rel="stylesheet">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <link href="<?php echo base_url("asset/css/sb-admin.css");?>" rel="stylesheet">

  <!--js-->
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>
  <div class="main">
    <div class="col-md-5 col-lg-5 hidden-xs hidden-sm" style="height:100%;">
      <img id="front" src="<?php echo base_url();?>asset/images/front.png">
    </div>
    <div class="back col-md-7 col-lg-7">
      <div class="col-md-8 col-md-offset-1">
        <img id="judul" style="margin-top: -40px;" src="<?php echo base_url();?>asset/images/logo.png">
      </div>

      <form action="<?php echo base_url(). 'index.php/Login'; ?>" method="post">

      <div class="col-md-8 col-md-offset-1" style="text-align:center;">
        <input type="text" class="form-control log" id="uname" placeholder="Username" name="username" style="width:70%; margin:0 auto;">
      </div>
      <div class="col-md-8 col-md-offset-1" style="text-align:center; margin-top:6%; margin-bottom: 2%;">
        <input type="password" class="form-control log" id="pwd" placeholder="Password" name="password" style="width:70%; margin:0 auto;">
      </div>
      <div class="col-md-8 col-md-offset-1">
        <h5><?php echo $this->session->flashdata('message');?></h5>
        <button class="myButton2">Login</button><br>
        </form>       
      </div>
      <div class="col-md-8 col-md-offset-1"><a href="<?php echo base_url(). 'index.php/Login/daftar'; ?>"><button class="myButton3">Daftar</button></a></div>
    </div>
  </div>
</body>
</html>