<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>sidg</title>

    <!-- Bootstrap Core CSS -->
    <!-- <link href="css/bootstrap.min.css" rel="stylesheet"> -->
    <link href="<?php echo base_url("asset/css/bootstrap.min.css");?>" rel="stylesheet">

    <!-- Custom CSS -->
    <!-- <link href="css/sb-admin.css" rel="stylesheet"> -->
    <link href="<?php echo base_url("asset/css/sb-admin.css");?>" rel="stylesheet">

    <!-- Morris Charts CSS -->
    <!-- link href="css/plugins/morris.css" rel="stylesheet"> -->
    <link href="<?php echo base_url("asset/css/plugins/morris.css");?>" rel="stylesheet">

    <!-- Custom Fonts -->
    <!-- <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"> -->
    <link href="<?php echo base_url("asset/font-awesome/css/font-awesome.min.css");?>" rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->

    </head>

    <body>

        <div id="wrapper">

            <!-- Navigation -->
            <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse" style="border-color:#00C853;" >
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar" style="background-color:#00C853;"></span>
                        <span class="icon-bar" style="background-color:#00C853;"></span>
                        <span class="icon-bar" style="background-color:#00C853;"></span>
                    </button>
                    <a class="navbar-left navbar-brand" href="d-daftar.html">
                        <img style="max-width:100px; margin-top:-25px; margin-left:-10px;" src="<?php echo base_url();?>asset/images/logo.png">
                    </a>
                </div>

                <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
                <div class="collapse navbar-collapse navbar-ex1-collapse">
                    <ul class="nav navbar-nav side-nav">
                        <div class="col-md-12">
                            <a href="" data-toggle="modal" data-target="#fotomodal">
                                <img id="ava" src="<?php 
                                $name = $this->session->userdata('foto');
                                if($name == ''){
                                    echo base_url('asset/images/ava.jpg');
                                }else{
                                    echo base_url($name);
                                }
                                ?>" style="margin-top:10%; margin-bottom:5%; background-color:white;">
                            </a>
                            <h5 class="judul" style="color:white;"><?php $name = $this->session->userdata('nama'); echo $name; ?></h5>
                        </div>

                        <li>
                            <a href="<?php echo base_url(); ?>index.php/admin/profile"><i class="fa fa-fw fa-user"></i> Profile</a>
                        </li>
                        <li  class="activee">
                            <a style="color:#13AB52;" href=""><i class="fa fa-fw fa-stethoscope"></i> Pelayanan Pasien</a>
                        </li>
                        <li>
                            <a href="<?php echo base_url(); ?>index.php/admin/tabel_Pasien"><i class="fa fa-fw fa-table"></i> Tabel Pasien</a>
                        </li>
                        <li>
                            <a href="<?php echo base_url(); ?>index.php/admin/tabel_perawatan"><i class="fa fa-fw fa-heart"></i> Tabel Perawatan</a>
                        </li>
                        <li>
                            <a href="<?php echo base_url(); ?>index.php/admin/laporan"><i class="fa fa-fw fa-folder-open"></i> Laporan</a>
                        </li>
                        <li>
                            <a href="<?php echo base_url(); ?>index.php/admin/tindakan"><i class="fa fa-fw fa-group"></i> Tindakan</a>
                        </li>
                        <li>
                            <a href="<?php echo base_url(); ?>index.php/admin/pegawai"><i class="fa fa-fw fa-group"></i> Daftar Pegawai</a>
                        </li>
                        <li>
                            <a href="<?php echo base_url(); ?>index.php/login/logout"><i class="fa fa-fw fa-sign-out"></i> Keluar</a>
                        </li>

                    </ul>
                </div>
                <!-- /.navbar-collapse -->
            </nav>

            <div id="page-wrapper">

                <div class="container-fluid" >

                    <!-- Page Heading -->
                    <div class="row" >
                        <div class="col-lg-12 judul">
                            <h1 class="page-header">
                            Odontogram
                            </h1>
                        </div>
                    </div>
                    <!-- /.row -->
                    <div class="row" style="margin-left:20px; margin-right:0px; white-space: nowrap; overflow-x: scroll hidden; ">
                        <div class="gigi-pilih" data-toggle="modal" data-target="#modalgigi" id="18" onclick="gigie(18)">
                                    <h4 >18</h4>
                                    <img style="width: 100%;" src="<?php echo base_url();?>asset/images/gigi.png" alt="card image">
                        </div>
                        <div class="gigi" data-toggle="modal" data-target="#modalgigi" onclick="gigie(17)">
                                    <h4 >17</h4>
                                    <img style="width: 100%;" src="<?php echo base_url();?>asset/images/gigi.png" alt="card image">
                        </div>
                          <div class="gigi" data-toggle="modal" data-target="#modalgigi" onclick="gigie(16)">
                                    <h4 >16</h4>
                                    <img style="width: 100%;" src="<?php echo base_url();?>asset/images/gigi.png" alt="card image">
                        </div>
                        <div class="gigi" data-toggle="modal" data-target="#modalgigi" onclick="gigie(15)">
                                    <h4 >15</h4>
                                    <img style="width: 100%;" src="<?php echo base_url();?>asset/images/gigi.png" alt="card image">
                        </div>
                         <div class="gigi" data-toggle="modal" data-target="#modalgigi" onclick="gigie(14)">
                                    <h4 >14</h4>
                                    <img style="width: 100%;" src="<?php echo base_url();?>asset/images/gigi.png" alt="card image">
                        </div>
                        <div class="gigi" data-toggle="modal" data-target="#modalgigi" onclick="gigie(13)">
                                    <h4 >13</h4>
                                    <img style="width: 100%;" src="<?php echo base_url();?>asset/images/gigi-2.png" alt="card image">
                        </div>
                          <div class="gigi-pilih" data-toggle="modal" data-target="#modalgigi" onclick="gigie(12)">
                                    <h4 >12</h4>
                                    <img style="width: 100%;" src="<?php echo base_url();?>asset/images/gigi-2.png" alt="card image">
                        </div>
                        <div class="gigi" data-toggle="modal" data-target="#modalgigi" onclick="gigie(11)" >
                                    <h4 >11</h4>
                                    <img style="width: 100%;" src="<?php echo base_url();?>asset/images/gigi-2.png" alt="card image">
                        </div>
                         <div class="gigi" data-toggle="modal" data-target="#modalgigi" style="margin-left: 10px;" onclick="gigie(21)">
                                    <h4 >21</h4>
                                    <img style="width: 100%;" src="<?php echo base_url();?>asset/images/gigi-2.png" alt="card image">
                        </div>
                        <div class="gigi" data-toggle="modal" data-target="#modalgigi" onclick="gigie(22)">
                                    <h4 >22</h4>
                                    <img style="width: 100%;" src="<?php echo base_url();?>asset/images/gigi-2.png" alt="card image">
                        </div>
                          <div class="gigi" data-toggle="modal" data-target="#modalgigi" onclick="gigie(23)">
                                    <h4 >23</h4>
                                    <img style="width: 100%;" src="<?php echo base_url();?>asset/images/gigi-2.png" alt="card image">
                        </div>
                        <div class="gigi" data-toggle="modal" data-target="#modalgigi" onclick="gigie(24)">
                                    <h4 >24</h4>
                                    <img style="width: 100%;" src="<?php echo base_url();?>asset/images/gigi.png" alt="card image">
                        </div>
                         <div class="gigi" data-toggle="modal" data-target="#modalgigi" onclick="gigie(25)">
                                    <h4 >25</h4>
                                    <img style="width: 100%;" src="<?php echo base_url();?>asset/images/gigi.png" alt="card image">
                        </div>
                        <div class="gigi" data-toggle="modal" data-target="#modalgigi" onclick="gigie(26)">
                                    <h4 >26</h4>
                                    <img style="width: 100%;" src="<?php echo base_url();?>asset/images/gigi.png" alt="card image">
                        </div>
                          <div class="gigi" data-toggle="modal" data-target="#modalgigi" onclick="gigie(27)">
                                    <h4 >27</h4>
                                    <img style="width: 100%;" src="<?php echo base_url();?>asset/images/gigi.png" alt="card image">
                        </div>
                         <div class="gigi" data-toggle="modal" data-target="#modalgigi" onclick="gigie(28)">
                                    <h4 >28</h4>
                                    <img style="width: 100%;" src="<?php echo base_url();?>asset/images/gigi.png" alt="card image">
                        </div>
                    </div>    
                    <div class="row" style="margin-left:215px; margin-right:0px; white-space: nowrap; overflow-x: scroll hidden; ">
                        <div class="gigi" data-toggle="modal" data-target="#modalgigi" onclick="gigie(55)">
                                    <h4 >55</h4>
                                    <img style="width: 100%;" src="<?php echo base_url();?>asset/images/gigi.png" alt="card image">
                        </div>
                        <div class="gigi" data-toggle="modal" data-target="#modalgigi" onclick="gigie(54)">
                                    <h4 >54</h4>
                                    <img style="width: 100%;" src="<?php echo base_url();?>asset/images/gigi.png" alt="card image">
                        </div>
                          <div class="gigi" data-toggle="modal" data-target="#modalgigi" onclick="gigie(53)">
                                    <h4 >53</h4>
                                    <img style="width: 100%;" src="<?php echo base_url();?>asset/images/gigi-2.png" alt="card image">
                        </div>
                        <div class="gigi" data-toggle="modal" data-target="#modalgigi" onclick="gigie(52)">
                                    <h4 >52</h4>
                                    <img style="width: 100%;" src="<?php echo base_url();?>asset/images/gigi-2.png" alt="card image">
                        </div>
                         <div class="gigi" data-toggle="modal" data-target="#modalgigi" onclick="gigie(51)">
                                    <h4 >51</h4>
                                    <img style="width: 100%;" src="<?php echo base_url();?>asset/images/gigi-2.png" alt="card image">
                        </div>
                        <div class="gigi" data-toggle="modal" data-target="#modalgigi" style="margin-left: 10px;" onclick="gigie(61)">
                                    <h4 >61</h4>
                                    <img style="width: 100%;" src="<?php echo base_url();?>asset/images/gigi-2.png" alt="card image">
                        </div>
                          <div class="gigi" data-toggle="modal" data-target="#modalgigi" onclick="gigie(62)">
                                    <h4 >62</h4>
                                    <img style="width: 100%;" src="<?php echo base_url();?>asset/images/gigi-2.png" alt="card image">
                        </div>
                        <div class="gigi" data-toggle="modal" data-target="#modalgigi" onclick="gigie(63)">
                                    <h4 >63</h4>
                                    <img style="width: 100%;" src="<?php echo base_url();?>asset/images/gigi-2.png" alt="card image">
                        </div>
                         <div class="gigi" data-toggle="modal" data-target="#modalgigi" onclick="gigie(64)">
                                    <h4 >64</h4>
                                    <img style="width: 100%;" src="<?php echo base_url();?>asset/images/gigi.png" alt="card image">
                        </div>
                        <div class="gigi" data-toggle="modal" data-target="#modalgigi" onclick="gigie(65)">
                                    <h4 >65</h4>
                                    <img style="width: 100%;" src="<?php echo base_url();?>asset/images/gigi.png" alt="card image">
                        </div>
                    </div>    
                     <div class="row" style="margin-left:215px; margin-right:0px; white-space: nowrap; overflow-x: scroll hidden; ">
                        <div class="gigi" data-toggle="modal" data-target="#modalgigi" onclick="gigie(85)">
                                    <img style="width: 100%;" src="<?php echo base_url();?>asset/images/gigi.png" alt="card image">
                                    <h4 >85</h4>
                        </div>
                        <div class="gigi" data-toggle="modal" data-target="#modalgigi" onclick="gigie(84)">
                                    <img style="width: 100%;" src="<?php echo base_url();?>asset/images/gigi.png" alt="card image">
                                    <h4 >84</h4>
                        </div>
                          <div class="gigi" data-toggle="modal" data-target="#modalgigi" onclick="gigie(83)">
                                    <img style="width: 100%;" src="<?php echo base_url();?>asset/images/gigi-2.png" alt="card image">
                                    <h4 >83</h4>
                        </div>
                        <div class="gigi" data-toggle="modal" data-target="#modalgigi" onclick="gigie(82)">
                                    <img style="width: 100%;" src="<?php echo base_url();?>asset/images/gigi-2.png" alt="card image">
                                    <h4 >82</h4>
                        </div>
                         <div class="gigi" data-toggle="modal" data-target="#modalgigi" onclick="gigie(81)">
                                    <img style="width: 100%;" src="<?php echo base_url();?>asset/images/gigi-2.png" alt="card image">
                                    <h4 >81</h4>
                        </div>
                        <div class="gigi" data-toggle="modal" data-target="#modalgigi" style="margin-left: 10px;" onclick="gigie(71)">
                                    <img style="width: 100%;" src="<?php echo base_url();?>asset/images/gigi-2.png" alt="card image">
                                    <h4 >71</h4>
                        </div>
                          <div class="gigi" data-toggle="modal" data-target="#modalgigi" onclick="gigie(72)">
                                    <img style="width: 100%;" src="<?php echo base_url();?>asset/images/gigi-2.png" alt="card image">
                                    <h4 >72</h4>
                        </div>
                        <div class="gigi" data-toggle="modal" data-target="#modalgigi" onclick="gigie(73)">
                                    <img style="width: 100%;" src="<?php echo base_url();?>asset/images/gigi-2.png" alt="card image">
                                    <h4 >73</h4>
                        </div>
                         <div class="gigi" data-toggle="modal" data-target="#modalgigi" onclick="gigie(74)">
                                    <img style="width: 100%;" src="<?php echo base_url();?>asset/images/gigi.png" alt="card image">
                                     <h4 >74</h4>
                        </div>
                        <div class="gigi" data-toggle="modal" data-target="#modalgigi" onclick="gigie(75)">
                                    <img style="width: 100%;" src="<?php echo base_url();?>asset/images/gigi.png" alt="card image">
                                     <h4 >75</h4>
                        </div>
                    </div> 

                     <div class="row" style="margin-left:20px; margin-right:0px; white-space: nowrap; overflow-x: scroll hidden; ">
                        <div class="gigi" data-toggle="modal" data-target="#modalgigi" onclick="gigie(48)">
                                    <img style="width: 100%;" src="<?php echo base_url();?>asset/images/gigi.png" alt="card image">
                                    <h4 >48</h4>
                        </div>
                        <div class="gigi" data-toggle="modal" data-target="#modalgigi" onclick="gigie(47)">
                                    <img style="width: 100%;" src="<?php echo base_url();?>asset/images/gigi.png" alt="card image">
                                    <h4 >47</h4>
                        </div>
                          <div class="gigi" data-toggle="modal" data-target="#modalgigi" onclick="gigie(46)">
                                    <img style="width: 100%;" src="<?php echo base_url();?>asset/images/gigi-2.png" alt="card image">
                                    <h4 >46</h4>
                        </div>
                        <div class="gigi" data-toggle="modal" data-target="#modalgigi" onclick="gigie(45)">
                                    <img style="width: 100%;" src="<?php echo base_url();?>asset/images/gigi-2.png" alt="card image">
                                    <h4 >45</h4>
                        </div>
                         <div class="gigi" data-toggle="modal" data-target="#modalgigi" onclick="gigie(44)">
                                    <img style="width: 100%;" src="<?php echo base_url();?>asset/images/gigi-2.png" alt="card image">
                                    <h4 >44</h4>
                        </div>
                        <div class="gigi" data-toggle="modal" data-target="#modalgigi" onclick="gigie(43)">
                                    <img style="width: 100%;" src="<?php echo base_url();?>asset/images/gigi-2.png" alt="card image">
                                    <h4 >43</h4>
                        </div>
                          <div class="gigi" data-toggle="modal" data-target="#modalg42i" onclick="gigie(42)">
                                    <img style="width: 100%;" src="<?php echo base_url();?>asset/images/gigi-2.png" alt="card image">
                                    <h4 >42</h4>
                        </div>
                        <div class="gigi" data-toggle="modal" data-target="#modalgigi" onclick="gigie(41)">
                                    <img style="width: 100%;" src="<?php echo base_url();?>asset/images/gigi-2.png" alt="card image">
                                    <h4 >41</h4>
                        </div>
                         <div class="gigi" data-toggle="modal" data-target="#modalgigi" style="margin-left: 10px;" onclick="gigie(31)">
                                    <img style="width: 100%;" src="<?php echo base_url();?>asset/images/gigi.png" alt="card image">
                                     <h4 >31</h4>
                        </div>
                        <div class="gigi" data-toggle="modal" data-target="#modalgigi" onclick="gigie(32)">
                                    <img style="width: 100%;" src="<?php echo base_url();?>asset/images/gigi.png" alt="card image">
                                     <h4 >32</h4>
                        </div>
                        <div class="gigi" data-toggle="modal" data-target="#modalgigi" onclick="gigie(33)">
                                    <img style="width: 100%;" src="<?php echo base_url();?>asset/images/gigi-2.png" alt="card image">
                                    <h4 >33</h4>
                        </div>
                        <div class="gigi" data-toggle="modal" data-target="#modalgigi" onclick="gigie(34)">
                                    <img style="width: 100%;" src="<?php echo base_url();?>asset/images/gigi-2.png" alt="card image">
                                    <h4 >34</h4>
                        </div>
                          <div class="gigi" data-toggle="modal" data-target="#modalgigi" onclick="gigie(35)">
                                    <img style="width: 100%;" src="<?php echo base_url();?>asset/images/gigi-2.png" alt="card image">
                                    <h4 >35</h4>
                        </div>
                        <div class="gigi" data-toggle="modal" data-target="#modalgigi" onclick="gigie(36)">
                                    <img style="width: 100%;" src="<?php echo base_url();?>asset/images/gigi-2.png" alt="card image">
                                    <h4 >36</h4>
                        </div>
                         <div class="gigi" data-toggle="modal" data-target="#modalgigi" onclick="gigie(37)">
                                    <img style="width: 100%;" src="<?php echo base_url();?>asset/images/gigi.png" alt="card image">
                                     <h4 >37</h4>
                        </div>
                        <div class="gigi" data-toggle="modal" data-target="#modalgigi" onclick="gigie(38)">
                                    <img style="width: 100%;" src="<?php echo base_url();?>asset/images/gigi.png" alt="card image">
                                     <h4 >38</h4>
                        </div>
                        <div class="row" style="margin-left: 10px; margin-top: 20px; color: black;">
                            <form class="col-lg-5 col-md-5 col-sm-12 col-xs-12" action="<?php echo base_url(). 'index.php/admin/inputgigiFinish/'; ?>" method="post">
                                        <label for="fname">Occlusi</label>
                                        <select class="form-control data" name="occlusi" style="margin-bottom: 10px; width: 100%;">
                                           <option hidden>-- Please Select -- </option>
                                           <option>Normal Bite</option>
                                           <option>Cross Bite</option>
                                           <option>Steep Bite</option>
                                       </select>

                                        <label for="ktp">Torus Palatinus</label>
                                        <select class="form-control data" name="torusP" style="margin-bottom: 10px; width: 100%; ">
                                           <option hidden>-- Please Select -- </option>
                                           <option>Tidak Ada</option>
                                           <option>Sedang</option>
                                           <option>Kecil</option>
                                           <option>Multiple</option>
                                       </select>

                                        <label for="ktp">Torus Mandibularis</label>
                                        <select class="form-control data" name="torusM" style="margin-bottom: 10px; width: 100%;">
                                           <option hidden>-- Please Select -- </option>
                                           <option>Tidak Ada</option>
                                           <option>Sisi Kiri</option>
                                           <option>Sisi Kanan</option>
                                           <option>Kedua Sisi</option>
                                       </select>

                                        <label for="ktp">Palatum</label>
                                        <select class="form-control data" name="palatum" style="margin-bottom: 10px; width: 100%;">
                                           <option hidden>-- Please Select -- </option>
                                           <option>Dalam</option>
                                           <option>Sedang</option>
                                           <option>Rendah</option>
                                       </select>

                                       <label for="ktp">Diastema</label>
                                        <select class="form-control data" name="diastema" style="margin-bottom: 10px; width: 100%;">
                                           <option hidden>-- Please Select -- </option>
                                           <option>Tidak Ada</option>
                                           <option>Ada</option>
                                       </select>

                                       <label for="ktp">Gigi Anomali</label>
                                        <select class="form-control data" name="gigi" style="margin-bottom: 10px; width: 100%;">
                                           <option hidden>-- Please Select -- </option>
                                           <option>Tidak Ada</option>
                                           <option>Ada</option>
                                       </select>

                                      
                        </div>
                        <div class="row" style="margin-left: 10%;">
                            
                                <button class="myButton" type="submit">Selesai</button>
                            </form>
                        </div>
                    </div>       

            </div>
            <!-- /#page-wrapper -->

        </div>

        <!-- /#wrapper -->

 <!-- Modal -->
            <div class="modal fade" id="modalgigi" role="dialog">
                <div class="modal-dialog modal-lg" >
                    <form id="forme" action="<?php echo base_url(). 'index.php/admin/inputgigi/'; ?>" method="post">
                  <!-- Modal content-->
                  <div class="modal-content" >
                    <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal">&times;</button>
                      <h4 class="modal-title">Info Gigi</h4>
                  </div>
                  <div class="modal-body" style="overflow-x: scroll;">
                    
                      <table class="table table-striped">
                        <thead style="align: center; background-color:#00C853; color:white;">
                            <tr>
                                <th >Kode Gigi</th>
                                <th style="vertical-align: middle;">Penyakit</th>
                                <th style="vertical-align: middle;">Restorasi</th>
                                <th style="vertical-align: middle;">Protesa</th>
                                <th style="vertical-align: middle;">Kode ICD 10</th>
                                <th style="vertical-align: middle;">Tindakan</th>
                                
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td><p id="gigiM"></p></td>
                                <td>
                                    <select class="form-control datamodal" name="penyakitM" style="margin-bottom: 10px;">
                                           <option value="Tidak Ada">Tidak Ada</option>
                                           <option value="Normal (sou)">Normal (sou)</option>
                                           <option value="Tidak Diketahui (non)">Tidak Diketahui (non)</option>
                                           <option value="Un-erupted (une)">Un-erupted (une)</option>
                                           <option value="Partial Erupted (pre)">Partial Erupted (pre)</option>
                                           <option value="Impacted Visible (imv)">Impacted Visible (imv)</option>
                                           <option value="Anomali (ano)">Anomali (ano)</option>
                                           <option value="Diastema (dia)">Diastema (dia)</option>
                                           <option value="Abrasi (abr)">Abrasi (abr)</option>
                                           <option value="Caries (car)">Caries (car)</option>
                                           <option value="Crown Fracture (cfr)">Crown Fracture (cfr)</option>
                                           <option value="Gigi Non Vital (nvt)">Gigi Non Vital (nvt)</option>
                                           <option value="Sisa Akar (rrx)">Sisa Akar (rrx)</option>
                                           <option value="Gigi Hilang (mis)">Gigi Hilang (mis)</option>
                                       </select>
                                </td>
                                <td>
                                    <select class="form-control datamodal" name="restorasiM" style="margin-bottom: 10px;">
                                           <option value="Tidak Ada">Tidak Ada</option>
                                           <option value="Amalgam Filling (amf)">Amalgam Filling (amf)</option>
                                           <option value="Silika (gif)">Silika (gif)</option>
                                           <option value="Composite Filling (cof)">Composite Filling (cof)</option>
                                           <option value="Fissure Sealant (fis)">Fissure Sealant (fis)</option>
                                           <option value="Inlay (inl)">Inlay (inl)</option>
                                           <option value="Full Metal Crown (fmc)">Full Metal Crown (fmc)</option>
                                           <option value="Metal Porcelain Crown (mpc)">Metal Porcelain Crown (mpc)</option>
                                           <option value="Gold Metal Crown (gmc)">Gold Metal Crown (gmc)</option>
                                           <option value="Root Canal Treatment (rct)">Root Canal Treatment (rct)</option>
                                           <option value="Implan (ipx)">Implan (ipx)</option>
                                           <option value="Metal Bridge (meb)">Metal Bridge (meb)</option>
                                           <option value="Porcelain Bridge (pob)">Porcelain Bridge (pob)</option>
                                           <option value="Pontic (pon)">Pontic (pon)</option>
                                           <option value="Gigi Abutment (abu)">Gigi Abutment (abu)</option>
                                       </select>
                                </td>
                                <td><select class="form-control datamodal" name="protesaM" style="margin-bottom: 10px;"> 
                                           <option value="Tidak Ada">Tidak Ada</option>
                                           <option value="Partial Denture (prd)">Partial Denture (prd)</option>
                                           <option value="Full Denture (fld)">Full Denture (fld)</option>
                                           <option value="Acrilic (acr)">Acrilic (acr)</option>
                                       </select></td>
                                <td>
                                      <input class="form-control datamodal" type="text" id="fname" name="icdm" placeholder="masukkan kode ICD 10">
                                         </td>
                                <td>
                                      <select class="form-control datamodal" name="tindakanm" style="margin-bottom: 10px;"> 
                                           <option value="Tidak Ada">Pilih</option>
                                           <?php      
                                            foreach($hasil as $b)
                                            {
                                                echo "<option value='$b->tindakan'>$b->tindakan</option>";
                                        }
                                        ?>
                                       </select>
                                </td>
                                <!-- <td>
                                      <input class="form-control datamodal" type="text" id="fname" name="firstname" placeholder="biaya">
                                         </td> -->
                            </tr>
                                <td><p id="gigiO"></p></td>
                                <td>
                                    <select class="form-control datamodal" name="penyakitO" style="margin-bottom: 10px;">
                                           <option value="Tidak Ada">Tidak Ada</option>
                                           <option value="Normal (sou)">Normal (sou)</option>
                                           <option value="Tidak Diketahui (non)">Tidak Diketahui (non)</option>
                                           <option value="Un-erupted (une)">Un-erupted (une)</option>
                                           <option value="Partial Erupted (pre)">Partial Erupted (pre)</option>
                                           <option value="Impacted Visible (imv)">Impacted Visible (imv)</option>
                                           <option value="Anomali (ano)">Anomali (ano)</option>
                                           <option value="Diastema (dia)">Diastema (dia)</option>
                                           <option value="Abrasi (abr)">Abrasi (abr)</option>
                                           <option value="Caries (car)">Caries (car)</option>
                                           <option value="Crown Fracture (cfr)">Crown Fracture (cfr)</option>
                                           <option value="Gigi Non Vital (nvt)">Gigi Non Vital (nvt)</option>
                                           <option value="Sisa Akar (rrx)">Sisa Akar (rrx)</option>
                                           <option value="Gigi Hilang (mis)">Gigi Hilang (mis)</option>
                                       </select>
                                </td>
                                <td>
                                    <select class="form-control datamodal" name="restorasiO" style="margin-bottom: 10px;">
                                           <option value="Tidak Ada">Tidak Ada</option>
                                           <option value="Amalgam Filling (amf)">Amalgam Filling (amf)</option>
                                           <option value="Silika (gif)">Silika (gif)</option>
                                           <option value="Composite Filling (cof)">Composite Filling (cof)</option>
                                           <option value="Fissure Sealant (fis)">Fissure Sealant (fis)</option>
                                           <option value="Inlay (inl)">Inlay (inl)</option>
                                           <option value="Full Metal Crown (fmc)">Full Metal Crown (fmc)</option>
                                           <option value="Metal Porcelain Crown (mpc)">Metal Porcelain Crown (mpc)</option>
                                           <option value="Gold Metal Crown (gmc)">Gold Metal Crown (gmc)</option>
                                           <option value="Root Canal Treatment (rct)">Root Canal Treatment (rct)</option>
                                           <option value="Implan (ipx)">Implan (ipx)</option>
                                           <option value="Metal Bridge (meb)">Metal Bridge (meb)</option>
                                           <option value="Porcelain Bridge (pob)">Porcelain Bridge (pob)</option>
                                           <option value="Pontic (pon)">Pontic (pon)</option>
                                           <option value="Gigi Abutment (abu)">Gigi Abutment (abu)</option>
                                       </select>
                                </td>
                                <td><select class="form-control datamodal" name="protesaO" style="margin-bottom: 10px;"> 
                                           <option value="Tidak Ada">Tidak Ada</option>
                                           <option value="Partial Denture (prd)">Partial Denture (prd)</option>
                                           <option value="Full Denture (fld)">Full Denture (fld)</option>
                                           <option value="Acrilic (acr)">Acrilic (acr)</option>
                                       </select></td>
                                <td>
                                      <input class="form-control datamodal" type="text" id="fname" name="icdo" placeholder="masukkan kode ICD 10">
                                         </td>
                                <td>
                                      <select class="form-control datamodal" name="tindakano" style="margin-bottom: 10px;"> 
                                           <option value="Tidak Ada">Pilih</option>
                                           <?php      
                                            foreach($hasil as $b)
                                            {
                                                echo "<option value='$b->tindakan'>$b->tindakan</option>";
                                        }
                                        ?>
                                       </select>
                                </td>
                                <!-- <td>
                                      <input class="form-control datamodal" type="text" id="fname" name="firstname" placeholder="biaya">
                                         </td> -->
                            </tr>
                                <td><p id="gigiD"></p></td>
                                <td>
                                    <select class="form-control datamodal" name="penyakitD"  style="margin-bottom: 10px;">
                                           <option value="Tidak Ada">Tidak Ada</option>
                                           <option value="Normal (sou)">Normal (sou)</option>
                                           <option value="Tidak Diketahui (non)">Tidak Diketahui (non)</option>
                                           <option value="Un-erupted (une)">Un-erupted (une)</option>
                                           <option value="Partial Erupted (pre)">Partial Erupted (pre)</option>
                                           <option value="Impacted Visible (imv)">Impacted Visible (imv)</option>
                                           <option value="Anomali (ano)">Anomali (ano)</option>
                                           <option value="Diastema (dia)">Diastema (dia)</option>
                                           <option value="Abrasi (abr)">Abrasi (abr)</option>
                                           <option value="Caries (car)">Caries (car)</option>
                                           <option value="Crown Fracture (cfr)">Crown Fracture (cfr)</option>
                                           <option value="Gigi Non Vital (nvt)">Gigi Non Vital (nvt)</option>
                                           <option value="Sisa Akar (rrx)">Sisa Akar (rrx)</option>
                                           <option value="Gigi Hilang (mis)">Gigi Hilang (mis)</option>
                                       </select>
                                </td>
                                <td>
                                    <select class="form-control datamodal" name="restorasiD" style="margin-bottom: 10px;">
                                           <option value="Tidak Ada">Tidak Ada</option>
                                           <option value="Amalgam Filling (amf)">Amalgam Filling (amf)</option>
                                           <option value="Silika (gif)">Silika (gif)</option>
                                           <option value="Composite Filling (cof)">Composite Filling (cof)</option>
                                           <option value="Fissure Sealant (fis)">Fissure Sealant (fis)</option>
                                           <option value="Inlay (inl)">Inlay (inl)</option>
                                           <option value="Full Metal Crown (fmc)">Full Metal Crown (fmc)</option>
                                           <option value="Metal Porcelain Crown (mpc)">Metal Porcelain Crown (mpc)</option>
                                           <option value="Gold Metal Crown (gmc)">Gold Metal Crown (gmc)</option>
                                           <option value="Root Canal Treatment (rct)">Root Canal Treatment (rct)</option>
                                           <option value="Implan (ipx)">Implan (ipx)</option>
                                           <option value="Metal Bridge (meb)">Metal Bridge (meb)</option>
                                           <option value="Porcelain Bridge (pob)">Porcelain Bridge (pob)</option>
                                           <option value="Pontic (pon)">Pontic (pon)</option>
                                           <option value="Gigi Abutment (abu)">Gigi Abutment (abu)</option>
                                       </select>
                                </td>
                                <td><select class="form-control datamodal" name="protesaD" style="margin-bottom: 10px;"> 
                                           <option value="Tidak Ada">Tidak Ada</option>
                                           <option value="Partial Denture (prd)">Partial Denture (prd)</option>
                                           <option value="Full Denture (fld)">Full Denture (fld)</option>
                                           <option value="Acrilic (acr)">Acrilic (acr)</option>
                                       </select></td>
                                <td>
                                      <input class="form-control datamodal" type="text" id="fname" name="icdd" placeholder="masukkan kode ICD 10">
                                         </td>
                                <td>
                                      <select class="form-control datamodal" name="tindakand" style="margin-bottom: 10px;"> 
                                           <option value="Tidak Ada">Pilih</option>
                                           <?php      
                                            foreach($hasil as $b)
                                            {
                                                echo "<option value='$b->tindakan'>$b->tindakan</option>";
                                        }
                                        ?>
                                       </select>
                                </td>
                               <!--  <td>
                                      <input class="form-control datamodal" type="text" id="fname" name="firstname" placeholder="biaya">
                                         </td> -->
                            </tr>
                             </tr>
                                <td><p id="gigiV"></p></td>
                                <td>
                                    <select class="form-control datamodal" name="penyakitV" id="ipenyakitM" style="margin-bottom: 10px;">
                                           <option value="Tidak Ada">Tidak Ada</option>
                                           <option value="Normal (sou)">Normal (sou)</option>
                                           <option value="Tidak Diketahui (non)">Tidak Diketahui (non)</option>
                                           <option value="Un-erupted (une)">Un-erupted (une)</option>
                                           <option value="Partial Erupted (pre)">Partial Erupted (pre)</option>
                                           <option value="Impacted Visible (imv)">Impacted Visible (imv)</option>
                                           <option value="Anomali (ano)">Anomali (ano)</option>
                                           <option value="Diastema (dia)">Diastema (dia)</option>
                                           <option value="Abrasi (abr)">Abrasi (abr)</option>
                                           <option value="Caries (car)">Caries (car)</option>
                                           <option value="Crown Fracture (cfr)">Crown Fracture (cfr)</option>
                                           <option value="Gigi Non Vital (nvt)">Gigi Non Vital (nvt)</option>
                                           <option value="Sisa Akar (rrx)">Sisa Akar (rrx)</option>
                                           <option value="Gigi Hilang (mis)">Gigi Hilang (mis)</option>
                                       </select>
                                </td>
                                <td>
                                    <select class="form-control datamodal" name="restorasiV" style="margin-bottom: 10px;">
                                           <option value="Tidak Ada">Tidak Ada</option>
                                           <option value="Amalgam Filling (amf)">Amalgam Filling (amf)</option>
                                           <option value="Silika (gif)">Silika (gif)</option>
                                           <option value="Composite Filling (cof)">Composite Filling (cof)</option>
                                           <option value="Fissure Sealant (fis)">Fissure Sealant (fis)</option>
                                           <option value="Inlay (inl)">Inlay (inl)</option>
                                           <option value="Full Metal Crown (fmc)">Full Metal Crown (fmc)</option>
                                           <option value="Metal Porcelain Crown (mpc)">Metal Porcelain Crown (mpc)</option>
                                           <option value="Gold Metal Crown (gmc)">Gold Metal Crown (gmc)</option>
                                           <option value="Root Canal Treatment (rct)">Root Canal Treatment (rct)</option>
                                           <option value="Implan (ipx)">Implan (ipx)</option>
                                           <option value="Metal Bridge (meb)">Metal Bridge (meb)</option>
                                           <option value="Porcelain Bridge (pob)">Porcelain Bridge (pob)</option>
                                           <option value="Pontic (pon)">Pontic (pon)</option>
                                           <option value="Gigi Abutment (abu)">Gigi Abutment (abu)</option>
                                       </select>
                                </td>
                                <td><select class="form-control datamodal" name="protesaV" style="margin-bottom: 10px;"> 
                                           <option value="Tidak Ada">Tidak Ada</option>
                                           <option value="Partial Denture (prd)">Partial Denture (prd)</option>
                                           <option value="Full Denture (fld)">Full Denture (fld)</option>
                                           <option value="Acrilic (acr)">Acrilic (acr)</option>
                                       </select></td>
                                <td>
                                      <input class="form-control datamodal" type="text" id="fname" name="icdv" placeholder="masukkan kode ICD 10">
                                         </td>
                                <td>
                                      <select class="form-control datamodal" name="tindakanv" style="margin-bottom: 10px;"> 
                                           <option value="Tidak Ada">Pilih</option>
                                           <?php      
                                            foreach($hasil as $b)
                                            {
                                                echo "<option value='$b->tindakan'>$b->tindakan</option>";
                                        }
                                        ?>
                                       </select>
                                </td>
                               <!--  <td>
                                      <input class="form-control datamodal" type="text" id="fname" name="firstname" placeholder="biaya">
                                         </td> -->
                            </tr>
                             </tr>
                                <td><p id="gigiL"></p></td>
                                <td>
                                    <select class="form-control datamodal" name="penyakitL" style="margin-bottom: 10px;">
                                           <option value="Tidak Ada">Tidak Ada</option>
                                           <option value="Normal (sou)">Normal (sou)</option>
                                           <option value="Tidak Diketahui (non)">Tidak Diketahui (non)</option>
                                           <option value="Un-erupted (une)">Un-erupted (une)</option>
                                           <option value="Partial Erupted (pre)">Partial Erupted (pre)</option>
                                           <option value="Impacted Visible (imv)">Impacted Visible (imv)</option>
                                           <option value="Anomali (ano)">Anomali (ano)</option>
                                           <option value="Diastema (dia)">Diastema (dia)</option>
                                           <option value="Abrasi (abr)">Abrasi (abr)</option>
                                           <option value="Caries (car)">Caries (car)</option>
                                           <option value="Crown Fracture (cfr)">Crown Fracture (cfr)</option>
                                           <option value="Gigi Non Vital (nvt)">Gigi Non Vital (nvt)</option>
                                           <option value="Sisa Akar (rrx)">Sisa Akar (rrx)</option>
                                           <option value="Gigi Hilang (mis)">Gigi Hilang (mis)</option>
                                       </select>
                                </td>
                                <td>
                                    <select class="form-control datamodal" name="restorasiL" style="margin-bottom: 10px;">
                                           <option value="Tidak Ada">Tidak Ada</option>
                                           <option value="Amalgam Filling (amf)">Amalgam Filling (amf)</option>
                                           <option value="Silika (gif)">Silika (gif)</option>
                                           <option value="Composite Filling (cof)">Composite Filling (cof)</option>
                                           <option value="Fissure Sealant (fis)">Fissure Sealant (fis)</option>
                                           <option value="Inlay (inl)">Inlay (inl)</option>
                                           <option value="Full Metal Crown (fmc)">Full Metal Crown (fmc)</option>
                                           <option value="Metal Porcelain Crown (mpc)">Metal Porcelain Crown (mpc)</option>
                                           <option value="Gold Metal Crown (gmc)">Gold Metal Crown (gmc)</option>
                                           <option value="Root Canal Treatment (rct)">Root Canal Treatment (rct)</option>
                                           <option value="Implan (ipx)">Implan (ipx)</option>
                                           <option value="Metal Bridge (meb)">Metal Bridge (meb)</option>
                                           <option value="Porcelain Bridge (pob)">Porcelain Bridge (pob)</option>
                                           <option value="Pontic (pon)">Pontic (pon)</option>
                                           <option value="Gigi Abutment (abu)">Gigi Abutment (abu)</option>
                                       </select>
                                </td>
                                <td><select class="form-control datamodal" name="protesaL" style="margin-bottom: 10px;"> 
                                           <option value="Tidak Ada">Tidak Ada</option>
                                           <option value="Partial Denture (prd)">Partial Denture (prd)</option>
                                           <option value="Full Denture (fld)">Full Denture (fld)</option>
                                           <option value="Acrilic (acr)">Acrilic (acr)</option>
                                       </select></td>
                                <td>
                                      <input class="form-control datamodal" type="text" id="fname" name="icdl" placeholder="masukkan kode ICD 10">
                                         </td>
                                <td>
                                      <select class="form-control datamodal" name="tindakanl" style="margin-bottom: 10px;"> 
                                           <option value="Tidak Ada">Pilih</option>
                                           <?php      
                                            foreach($hasil as $b)
                                            {
                                                echo "<option value='$b->tindakan'>$b->tindakan</option>";
                                        }
                                        ?>
                                       </select>
                                </td>
                               <!--  <td>
                                      <input class="form-control datamodal" type="text" id="fname" name="firstname" placeholder="biaya">
                                         </td> -->
                            </tr>
                        </tbody>
                    </table>
                    
                    </div>
                    <div class="modal-footer">
                        
                      <button type="submit" class="btn btn-success" >Simpan</button>
                  </div>
              </div>
              </form>
          </div>
      </div>

            <!-- Modal -->
<div class="modal fade" id="fotomodal" role="dialog">
    <div class="modal-dialog">

      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Edit Foto</h4>
      </div>
      <div class="modal-body">
        <form method="post" action="<?php echo base_url(). 'index.php/admin/foto'; ?>" enctype="multipart/form-data">
          <label>Masukkan foto</label> 
          <input style="border:none;" class="form-control data" type="file" id="lname" name="foto">
      </form>
  </div>
  <div class="modal-footer">
      <button type="submit" class="btn btn-default" >save</button>
  </div>
</div>

</div>
</div>

<!-- /Modal-->

      <!-- /Modal-->

      <!-- Modal -->
<!--             <div class="modal fade" id="ubahgigiM" role="dialog">
                <div class="modal-dialog">


                  <div class="modal-content">
                    <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal">&times;</button>
                      <h4 class="modal-title">Info Gigi</h4>
                  </div>
                  <div class="modal-body">
                         <form action="/action_page.php">
                                        <label for="fname">Kode Gigi</label>
                                       <select class="form-control data" style="margin-bottom: 10px;">
                                           <option selected> <p id="kodene">M</p></option>
                                       </select>

                                         <label for="fname">Penyakit</label>
                                       <select class="form-control data" id="ipenyakitM" style="margin-bottom: 10px;">
                                           <option>Tidak Ada</option>
                                           <option>Normal (sou)</option>
                                           <option>Tidak Diketahui (non)</option>
                                           <option>Un-erupted (une)</option>
                                           <option>Partial Erupted (pre)</option>
                                           <option>Impacted Visible (imv)</option>
                                           <option>Anomali (ano)</option>
                                           <option>Diastema (dia)</option>
                                           <option>Abrasi (abr)</option>
                                           <option>Caries (car)</option>
                                           <option>Crown Fracture (cfr)</option>
                                           <option>Gigi Non Vital (nvt)</option>
                                           <option>Sisa Akar (rrx)</option>
                                           <option>Gigi Hilang (mis)</option>
                                       </select>

                                       <label for="fname">Restorasi</label>
                                       <select class="form-control data" id="irestorasiM" style="margin-bottom: 10px;">
                                           <option>Tidak Ada</option>
                                           <option>Amalgam Filling (amf)</option>
                                           <option>Silika (gif)</option>
                                           <option>Composite Filling (cof)</option>
                                           <option>Fissure Sealant (fis)</option>
                                           <option>Inlay (inl)</option>
                                           <option>Full Metal Crown (fmc)</option>
                                           <option>Porcelain Crown (poc)</option>
                                           <option>Metal Porcelain Crown (mpc)</option>
                                           <option>Gold Metal Crown (gmc)</option>
                                           <option>Root Canal Treatment (rct)</option>
                                           <option>Implan (ipx)</option>
                                           <option>Metal Bridge (meb)</option>
                                           <option>Porcelain Bridge (pob)</option>
                                           <option>Pontic (pon)</option>
                                           <option>Gigi Abutment (abu)</option>
                                       </select>

                                        <label for="fname">Protesa</label>
                                       <select class="form-control data" id="iprotesaM" style="margin-bottom: 10px;"> 
                                           <option>Tidak Ada</option>
                                           <option>Partial Denture (prd)</option>
                                           <option>Full Denture (fld)</option>
                                           <option>Acrilic (acr)</option>
                                       </select>

                                         <label for="fname">Kode ICD 10</label>
                                      <input class="form-control data" type="text" id="fname" name="firstname" placeholder="masukkan kode ICD 10">

                                         <label for="fname">Penanganan</label>
                                      <input class="form-control data" type="text" id="fname" name="firstname" placeholder="masukkan penanganan">
                        </form>

                    </div>
                    <div class="modal-footer">
                      <button type="button" onclick="myFunctionM()" class="btn btn-success" data-dismiss="modal">Simpan</button>
                  </div>
              </div>

          </div>
      </div> -->


      <!-- /Modal-->
    <script>
        function myFunctionM() {
            var x = document.getElementById("ipenyakitM").value;
            document.getElementById("penyakitM").innerHTML = x;

            var y = document.getElementById("irestorasiM").value;
            document.getElementById("restorasiM").innerHTML = y;

            var z = document.getElementById("iprotesaM").value;
            document.getElementById("protesaM").innerHTML = z;
        }
        function myFunctionO() {
            var x = document.getElementById("ipenyakitO").value;
            document.getElementById("penyakitO").innerHTML = x;

            var y = document.getElementById("irestorasiO").value;
            document.getElementById("restorasiO").innerHTML = y;

            var z = document.getElementById("iprotesaO").value;
            document.getElementById("protesaO").innerHTML = z;
        }
        function myFunctionD() {
            var x = document.getElementById("ipenyakitD").value;
            document.getElementById("penyakitD").innerHTML = x;

            var y = document.getElementById("irestorasiD").value;
            document.getElementById("restorasiD").innerHTML = y;

            var z = document.getElementById("iprotesaD").value;
            document.getElementById("protesaD").innerHTML = z;
        }
        function myFunctionV() {
            var x = document.getElementById("ipenyakitV").value;
            document.getElementById("penyakitV").innerHTML = x;

            var y = document.getElementById("irestorasiV").value;
            document.getElementById("restorasiV").innerHTML = y;

            var z = document.getElementById("iprotesaV").value;
            document.getElementById("protesaV").innerHTML = z;
        }
        function myFunctionL() {
            var x = document.getElementById("ipenyakitL").value;
            document.getElementById("penyakitL").innerHTML = x;

            var y = document.getElementById("irestorasiL").value;
            document.getElementById("restorasiL").innerHTML = y;

            var z = document.getElementById("iprotesaL").value;
            document.getElementById("protesaL").innerHTML = z;
        }

        function gigie(x){
            document.getElementById("gigiM").innerHTML = x+'-M';
            document.getElementById("gigiO").innerHTML = x+'-O';
            document.getElementById("gigiD").innerHTML = x+'-D';
            document.getElementById("gigiV").innerHTML = x+'-V';
            document.getElementById("gigiL").innerHTML = x+'-L';
            document.getElementById("forme").action = "<?php echo base_url(). 'index.php/admin/inputgigi/'; ?>"+x;

        }
        function inputan(){
            var x = document.getElementById("gigiM").value;
            document.getElementById("kodene").innerHTML = 'he';
        }

    </script>                                                                  
     
<!-- jQuery -->
        <script src="<?php echo base_url("asset/js/jquery.js");?>"></script>

        <!-- Bootstrap Core JavaScript -->
        <script src="<?php echo base_url("asset/js/bootstrap.min.js");?>"></script>

        <!-- Morris Charts JavaScript -->
        <script src="<?php echo base_url("asset/js/plugins/morris/raphael.min.js");?>"></script>
        <script src="<?php echo base_url("asset/js/plugins/morris/morris.min.js");?>"></script>
        <script src="<?php echo base_url("asset/js/plugins/morris/morris-data.js");?>"></script>

</body>

</html>
