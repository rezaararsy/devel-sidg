<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>sidg</title>

    <!-- Bootstrap Core CSS -->
    <!-- <link href="css/bootstrap.min.css" rel="stylesheet"> -->
    <link href="<?php echo base_url("asset/css/bootstrap.min.css");?>" rel="stylesheet">

    <!-- Custom CSS -->
    <!-- <link href="css/sb-admin.css" rel="stylesheet"> -->
    <link href="<?php echo base_url("asset/css/sb-admin.css");?>" rel="stylesheet">

    <!-- Morris Charts CSS -->
    <!-- link href="css/plugins/morris.css" rel="stylesheet"> -->
    <link href="<?php echo base_url("asset/css/plugins/morris.css");?>" rel="stylesheet">

    <!-- Custom Fonts -->
    <!-- <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"> -->
    <link href="<?php echo base_url("asset/font-awesome/css/font-awesome.min.css");?>" rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->

    </head>

    <body>

        <div id="wrapper">

            <!-- Navigation -->
            <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse" style="border-color:#00C853;" >
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar" style="background-color:#00C853;"></span>
                        <span class="icon-bar" style="background-color:#00C853;"></span>
                        <span class="icon-bar" style="background-color:#00C853;"></span>
                    </button>
                    <a class="navbar-left navbar-brand" href="d-daftar.html">
                        <img style="max-width:100px; margin-top:-25px; margin-left:-10px;" src="<?php echo base_url();?>asset/images/logo.png">
                    </a>
                </div>

                <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
                <div class="collapse navbar-collapse navbar-ex1-collapse">
                    <ul class="nav navbar-nav side-nav">
                        <div class="col-md-12">
                            <a href="" data-toggle="modal" data-target="#fotomodal">
                                <img id="ava" src="<?php 
                                $name = $this->session->userdata('foto');
                                if($name == ''){
                                    echo base_url('asset/images/ava.jpg');
                                }else{
                                    echo base_url($name);
                                }
                                ?>" style="margin-top:10%; margin-bottom:5%; background-color:white;">
                            </a>
                            <h5 class="judul" style="color:white;"><?php $name = $this->session->userdata('nama'); echo $name; ?></h5>
                        </div>

                        <li  class="activee">
                            <a style="color:#13AB52;" href="<?php echo base_url(); ?>index.php/admin/profile"><i class="fa fa-fw fa-user"></i> Profile</a>
                        </li>
                        <li>
                            <a href="<?php echo base_url(); ?>index.php/admin"><i class="fa fa-fw fa-stethoscope"></i> Pelayanan Pasien</a>
                        </li>
                        <li >
                            <a href="<?php echo base_url(); ?>index.php/admin/tabel_Pasien"><i class="fa fa-fw fa-table"></i> Tabel Pasien</a>
                        </li>
                        <li>
                            <a href="<?php echo base_url(); ?>index.php/admin/tabel_perawatan"><i class="fa fa-fw fa-heart"></i> Tabel Perawatan</a>
                        </li>
                        <li>
                            <a  href="<?php echo base_url(); ?>index.php/admin/laporan"><i class="fa fa-fw fa-folder-open"></i> Laporan</a>
                        </li>
                         <li>
                            <a href="<?php echo base_url(); ?>index.php/admin/tindakan"><i class="fa fa-fw fa-group"></i> Tindakan</a>
                        </li>
                        <li>
                            <a href="<?php echo base_url(); ?>index.php/admin/pegawai"><i class="fa fa-fw fa-group"></i> Daftar Pegawai</a>
                        </li>
                        <li>
                            <a href="<?php echo base_url(); ?>index.php/login/logout"><i class="fa fa-fw fa-sign-out"></i> Keluar</a>
                        </li>

                    </ul>
                </div>
                <!-- /.navbar-collapse -->
            </nav>

            <div id="page-wrapper">

                <div class="container-fluid">

                    <!-- Page Heading -->
                    <div class="row">
                        <div class="col-lg-12 judul">
                            <h1 class="page-header">
                                Profile
                            </h1>
                        </div>
                    </div>
                    <!-- /.row -->
                    <div class="row">
                        <div style="width: 400px; margin-left: 40px;" class="panel panel-default">
                            <div class="panel-heading"><b>Filter Seacrh</b>
                            </div>
                            <div style="color: black;" class="panel-body">
                                 <form>
                                    <label style="font-size: 1em;">Tanggal</label><br>
                                    <input  type="date" name="search">
                                    <span> - </span>
                                    <input type="date" name="search"><br>
                                    <label style="font-size: 1em;">Nama Pasien</label><br>
                                    <select style="margin-bottom: 10px; float: left; margin-right: 20px;" class="data form-control" name="jk" id="klinik">
                                            <option selected="">Semua Pasien </option>
                                            <option value="Ada">Seluruh Pasien</option>
                                            <option value="Tidak Ada">Lendy</option>
                                        </select>
                                    <button class="btn btn-success" style=" width: 90px; background-color:#00C853; border: none; ">Cari</button>
                        </form>
                            </div>
                    </div>
                    <div class="row" style="margin-bottom: 10px; margin-right: 5%; float: right;">
                    </div>
                </div>
                <!-- /#page-wrapper -->

            </div>

            <!-- /#wrapper -->

      <!-- Modal -->
<div class="modal fade" id="fotomodal" role="dialog">
    <div class="modal-dialog">

      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Edit Foto</h4>
      </div>
      <div class="modal-body">
        <form>
          <label>Masukkan foto</label> 
          <input style="border:none;" class="form-control data" type="file" id="lname" name="lastname">
      </form>
  </div>
  <div class="modal-footer">
      <button type="button" class="btn btn-default" data-dismiss="modal">save</button>
  </div>
</div>

</div>
</div>

<!-- /Modal-->



      <!-- jQuery -->
        <script src="<?php echo base_url("asset/js/jquery.js");?>"></script>

        <!-- Bootstrap Core JavaScript -->
        <script src="<?php echo base_url("asset/js/bootstrap.min.js");?>"></script>

        <!-- Morris Charts JavaScript -->
        <script src="<?php echo base_url("asset/js/plugins/morris/raphael.min.js");?>"></script>
        <script src="<?php echo base_url("asset/js/plugins/morris/morris.min.js");?>"></script>
        <script src="<?php echo base_url("asset/js/plugins/morris/morris-data.js");?>"></script>

  </body>

  </html>
