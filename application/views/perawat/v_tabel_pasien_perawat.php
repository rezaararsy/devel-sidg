<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>sidg</title>

    <!-- Bootstrap Core CSS -->
    <!-- <link href="css/bootstrap.min.css" rel="stylesheet"> -->
    <link href="<?php echo base_url("asset/css/bootstrap.min.css");?>" rel="stylesheet">

    <!-- Custom CSS -->
    <!-- <link href="css/sb-admin.css" rel="stylesheet"> -->
    <link href="<?php echo base_url("asset/css/sb-admin.css");?>" rel="stylesheet">

    <!-- Morris Charts CSS -->
    <!-- link href="css/plugins/morris.css" rel="stylesheet"> -->
    <link href="<?php echo base_url("asset/css/plugins/morris.css");?>" rel="stylesheet">

    <!-- Custom Fonts -->
    <!-- <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"> -->
    <link href="<?php echo base_url("asset/font-awesome/css/font-awesome.min.css");?>" rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->

    </head>

    <body>

        <div id="wrapper">

            <!-- Navigation -->
            <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse" style="border-color:#00C853;" >
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar" style="background-color:#00C853;"></span>
                        <span class="icon-bar" style="background-color:#00C853;"></span>
                        <span class="icon-bar" style="background-color:#00C853;"></span>
                    </button>
                    <a class="navbar-left navbar-brand" href="d-daftar.html">
                        <img style="max-width:100px; margin-top:-25px; margin-left:-10px;" src="<?php echo base_url();?>asset/images/logo.png">
                    </a>
                </div>

                <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
                <div class="collapse navbar-collapse navbar-ex1-collapse">
                    <ul class="nav navbar-nav side-nav">
                        <div class="col-md-12">
                            <a href="" data-toggle="modal" data-target="#fotomodal">
                                <img id="ava" src="<?php 
                                $name = $this->session->userdata('foto');
                                if($name == ''){
                                    echo base_url('asset/images/ava.jpg');
                                }else{
                                    echo base_url($name);
                                }
                                ?>" style="margin-top:10%; margin-bottom:5%; background-color:white;">
                            </a>
                            <h5 class="judul" style="color:white;"><?php $name = $this->session->userdata('nama'); echo $name; ?></h5>
                        </div>

                        <li>
                            <a href="<?php echo base_url(); ?>index.php/perawat/" ><i class="fa fa-fw fa-pencil"></i> Pendaftaran Pasien</a>
                        </li>
                        <li>
                            <a href="<?php echo base_url(); ?>index.php/perawat/antrian"><i class="fa fa-fw fa-stethoscope"></i> Antrian Pasien Pasien</a>
                        </li>
                        <li class="activee">
                            <a href="" style="color:#13AB52;"><i class="fa fa-fw fa-table"></i> Daftar Pasien</a>
                        </li>
                        <li>
                            <a href="<?php echo base_url(); ?>index.php/login/logout"><i class="fa fa-fw fa-sign-out"></i> Keluar</a>
                        </li>

                    </ul>
                </div>
                <!-- /.navbar-collapse -->
            </nav>

            <div id="page-wrapper">

                <div class="container-fluid">

                    <!-- Page Heading -->
                    <div class="row">
                        <div class="col-lg-12 judul">
                            <h1 class="page-header">
                                Tabel Pasien
                            </h1>
                        </div>
                    </div>
                    <!-- /.row -->
                    <div class="row">
                        <form>
                            <input class="search" type="text" name="search" placeholder="Search..">
                        </form>
                    </div>
                    <div class="row" style="margin-left:40px; margin-right:40px;">
                         <table class="table table-striped">
                        <thead style="align:center; background-color:#00C853; color:white;">
                            <tr>
                                <th style="width: 200px; vertical-align: middle;">Nama Pasien</th>
                                <th style="vertical-align: middle;">No KTP</th>
                                <th style="width:20px; vertical-align: middle;">Usia</th>
                                <th style="vertical-align: middle;">Jenis Kelamin</th>
                                <th style="vertical-align: middle;">Tempat, Tanggal Lahir</th>
                                <th style="vertical-align: middle;">Suku/Ras</th>
                                <th style="vertical-align: middle;">Pekerjaan</th>
                                <th style="vertical-align: middle;">Alamat</th>
                                <th style="vertical-align: middle;">No Telepon</th>
                                <th style="vertical-align: middle;">Data Medik</th>
                                <th style="vertical-align: middle;">Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php      
                            foreach($hasil as $b)
                            {
                                echo "<tr>
                                <td>$b->nama</td>
                                <td>$b->nik</td>
                                <td>$b->usia</td>
                                <td>$b->jk</td>
                                <td>$b->tgll</td>
                                <td>$b->suku</td>
                                <td>$b->pekerjaan</td>
                                <td>$b->alamat</td>
                                <td>$b->telp</td>
                                <td style='text-align: center;''><a href='' style='color:#00C853;' data-toggle='modal' data-target='#modalmedis'><span class='fa fa-2x fa-external-link'></span></a></td>
                                <td></td>
                            </tr>";
                        }
                        ?>                    
                        </tbody>
                    </table>
                    </div>      
                </div>
                <!-- /#page-wrapper -->

            </div>

            <!-- /#wrapper -->

<!-- Modal -->
            <div class="modal fade" id="modalmedis" role="dialog">
                <div class="modal-dialog modal-lg">

                  <!-- Modal content-->
                  <div class="modal-content">
                    <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal">&times;</button>
                      <h4 class="modal-title">Data Medis</h4>
                  </div>
                  <div class="modal-body">
                      <table class="table table-striped">
                        <thead style="align:center; background-color:#00C853; color:white;">
                            <tr>
                                <th>Gol. Darah</th>
                                <th>Tekanan Darah</th>
                                <th>Penyakit Jantung</th>
                                <th style="vertical-align: middle;">Diabetes</th>
                                <th style="vertical-align: middle;">Haemopilia</th>
                                <th style="vertical-align: middle;">Hepatitis</th>
                                <th style="vertical-align: middle;">Gastring</th>
                                <th style="vertical-align: middle;">Penyakit Lain</th>
                                <th style="vertical-align: middle;">Alergi Obat</th>
                                <th style="vertical-align: middle;">Alergi Makanan</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>A</td>
                                <td>80m</td>
                                <td>Tidak Ada</td>
                                <td>Tidak Ada</td>
                                <td>Tidak Ada</td>
                                <td>Tidak Ada</td>
                                <td>Tidak Ada</td>
                                <td>Tidak Ada</td>
                                <td>Tidak Ada</td>
                                <td>Tidak Ada</td>
                            </tr>
                        </tbody>
                    </table>

                    </div>
                    <div class="modal-footer">
                        
                      <button type="button" class="btn btn-success" data-dismiss="modal">OK</button>
                  </div>
              </div>

          </div>
      </div>

      <!-- /Modal-->

      <!-- Modal -->
<div class="modal fade" id="fotomodal" role="dialog">
    <div class="modal-dialog">

      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Edit Foto</h4>
      </div>
      <div class="modal-body">
        <form method="post" action="<?php echo base_url(). 'index.php/perawat/foto'; ?>" enctype="multipart/form-data">
          <label>Masukkan foto</label> 
          <input style="border:none;" class="form-control data" type="file" id="lname" name="foto">
      
  </div>
  <div class="modal-footer">
      <button type="submit" class="btn btn-default" >save</button>
  </div>
</div>
</form>

</div>
</div>

<!-- /Modal-->



      <!-- jQuery -->
        <script src="<?php echo base_url("asset/js/jquery.js");?>"></script>

        <!-- Bootstrap Core JavaScript -->
        <script src="<?php echo base_url("asset/js/bootstrap.min.js");?>"></script>

        <!-- Morris Charts JavaScript -->
        <script src="<?php echo base_url("asset/js/plugins/morris/raphael.min.js");?>"></script>
        <script src="<?php echo base_url("asset/js/plugins/morris/morris.min.js");?>"></script>
        <script src="<?php echo base_url("asset/js/plugins/morris/morris-data.js");?>"></script>

  </body>

  </html>
