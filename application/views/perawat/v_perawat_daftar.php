<!DOCTYPE html>
<html lang="en">
<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>sidg</title>

    <!-- Bootstrap Core CSS -->
    <!-- <link href="css/bootstrap.min.css" rel="stylesheet"> -->
    <link href="<?php echo base_url("asset/css/bootstrap.min.css");?>" rel="stylesheet">

    <!-- Custom CSS -->
    <!-- <link href="css/sb-admin.css" rel="stylesheet"> -->
    <link href="<?php echo base_url("asset/css/sb-admin.css");?>" rel="stylesheet">

    <!-- Morris Charts CSS -->
    <!-- link href="css/plugins/morris.css" rel="stylesheet"> -->
    <link href="<?php echo base_url("asset/css/plugins/morris.css");?>" rel="stylesheet">

    <!-- Custom Fonts -->
    <!-- <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"> -->
    <link href="<?php echo base_url("asset/font-awesome/css/font-awesome.min.css");?>" rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->

    </head>

    <body>

        <div id="wrapper">

            <!-- Navigation -->
            <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse" style="border-color:#00C853;" >
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar" style="background-color:#00C853;"></span>
                        <span class="icon-bar" style="background-color:#00C853;"></span>
                        <span class="icon-bar" style="background-color:#00C853;"></span>
                    </button>
                    <a class="navbar-left navbar-brand" href="d-daftar.html">
                        <img style="max-width:100px; margin-top:-25px; margin-left:-10px;" src="<?php echo base_url();?>asset/images/logo.png">
                    </a>
                </div>

                <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
                <div class="collapse navbar-collapse navbar-ex1-collapse">
                    <ul class="nav navbar-nav side-nav">
                        <div class="col-md-12">
                            <a href="" data-toggle="modal" data-target="#fotomodal">
                                <img id="ava" src="<?php 
                                $name = $this->session->userdata('foto');
                                if($name == ''){
                                    echo base_url('asset/images/ava.jpg');
                                }else{
                                    echo base_url($name);
                                }
                                ?>" style="margin-top:10%; margin-bottom:5%; background-color:white;">
                            </a>
                            <h5 class="judul" style="color:white;"><?php $name = $this->session->userdata('nama'); echo $name; ?></h5>
                        </div>

                        <li class="activee">
                            <a href="" style="color:#13AB52;"><i class="fa fa-fw fa-pencil"></i> Pendaftaran Pasien</a>
                        </li>
                        <li>
                            <a href="<?php echo base_url(); ?>index.php/perawat/antrian"><i class="fa fa-fw fa-stethoscope"></i> Antrian Pasien Pasien</a>
                        </li>
                        <li>
                            <a href="<?php echo base_url(); ?>index.php/perawat/tabel_pasien"><i class="fa fa-fw fa-table"></i> Daftar Pasien</a>
                        </li>
                        <li>
                            <a href="<?php echo base_url(); ?>index.php/login/logout"><i class="fa fa-fw fa-sign-out"></i> Keluar</a>
                        </li>

                    </ul>
                </div>
                <!-- /.navbar-collapse -->
            </nav>

            <div id="page-wrapper">

                <div class="container-fluid">

                    <!-- Page Heading -->
                    <div class="row">
                        <div class="col-lg-12 judul">
                            <h1 class="page-header">
                                Formulir Pendaftaran Pasien
                            </h1>
                        </div>
                    </div>
                    <!-- /.row -->
                <div class="row" style="margin-left:40px; margin-right:40px;">
                    <div class="panel-group" style="color:black;">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a data-toggle="collapse" href="#collapse1">Formulir Pendaftaran </a>
                                </h4>
                            </div>  
                            <div id="collapse1" class="panel-collapse collapse">
                                <div class="panel-body">
                                    <form action="<?php echo base_url(). 'index.php/perawat/inputData'; ?>" method="post">

                                        <label for="fname">Username</label>
                                        <input class="form-control data" type="text" id="fname" name="username" placeholder="masukkan username">

                                        <label for="fname">Password</label>
                                        <input class="form-control data" type="password" id="fname" name="password" placeholder="masukkan password">


                                        <label for="fname">Nama Pasien</label>
                                        <input class="form-control data" type="text" id="fname" name="nama" placeholder="masukkan nama pasien">

                                        <label for="ktp">No KTP</label>
                                        <input class="form-control data" type="text" id="lname" name="no_ktp" placeholder="masukkan no ktp">

                                        <label for="usia">Usia</label>
                                        <input class="form-control data" type="text" id="lname" name="usia" placeholder="masukkan usia pasien">

                                        <label for="usia">Jenis Kelamin</label>
                                        <select style="margin-bottom: 10px;" class="data form-control" name="jk" id="klinik">
                                            <option hidden>-- Please Select -- </option>
                                            <option value="Ada">Laki-laki</option>
                                            <option value="Tidak Ada">Perempuan</option>
                                        </select>

                                        <label for="ttl">Tempat Lahir</label>
                                        <input class="form-control data" type="text" id="lname" name="tmpl" placeholder="masukkan tempat lahir">

                                        <label for="tgl">Tanggal lahir</label>
                                        <input class="form-control data" type="date" id="lname" name="tgll" placeholder="masukkan tanggal lahir">


                                        <label for="suku">Suku/Ras</label>
                                        <input class="form-control data" type="text" id="lname" name="suku" placeholder="masukkan suka atau ras">

                                        <label for="kerja">Pekerjaan</label>
                                        <input class="form-control data" type="text" id="lname" name="pekerjaan" placeholder="masukkan pekerjaan">

                                        <label for="alamat">Alamat</label>
                                        <input class="form-control data" type="text" id="lname" name="alamat" placeholder="masukkan alamat">

                                        <label for="tlp">No Telepon</label>
                                        <input class="form-control data" type="text" id="lname" name="no_hp" placeholder="masukkan no telepon">
                                </div>
                            </div>
                        </div>
                    </div>  
                    <div class="panel-group" style="color:black;">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a data-toggle="collapse" href="#collapse2">Data Medik</a>
                                </h4>
                            </div>  
                            <div id="collapse2" class="panel-collapse collapse">
                                <div class="panel-body">
                                        <label for="fname">Golongan Darah</label>
                                       <select style="margin-bottom: 10px;" class="data form-control" name="gol_darah" id="klinik">
                                            <option hidden>-- Please Select -- </option>
                                            <option value="A">A</option>
                                            <option value="AB">AB</option>
                                            <option value="B">B</option>
                                            <option value="O">O</option>
                                        </select>

                                        <label for="ktp">Tekanan Darah</label>
                                        <input class="form-control data" type="text" id="lname" name="tkn_darah" placeholder="masukkan tekanan darah">

                                        <label for="usia">Penyakit Jantung</label>
                                        <select style="margin-bottom: 10px;" class="data form-control" name="pjantung" id="klinik">
                                            <option hidden>-- Please Select -- </option>
                                            <option value="Ada">Ada</option>
                                            <option value="Tidak Ada">Tidak Ada</option>
                                        </select>

                                        <label for="ttl">Diabetes</label>
                                        <select style="margin-bottom: 10px;" name="diabetes" class="data form-control" id="klinik">
                                            <option hidden>-- Please Select -- </option>
                                            <option value="Ada">Ada</option>
                                            <option value="Tidak Ada">Tidak Ada</option>
                                        </select>

                                        <label for="tgl">Haemopilia</label>
                                        <select style="margin-bottom: 10px;" name="haemo" class="data form-control" id="klinik">
                                            <option hidden>-- Please Select -- </option>
                                            <option value="Ada">Ada</option>
                                            <option value="Tidak Ada">Tidak Ada</option>
                                        </select>


                                        <label for="suku">Hepatitis</label>
                                        <select style="margin-bottom: 10px;" name="hepa" class="data form-control" id="klinik">
                                            <option hidden>-- Please Select -- </option>
                                            <option value="Ada">Ada</option>
                                            <option value="Tidak Ada">Tidak Ada</option>
                                        </select>

                                        <label for="kerja">Gastring</label>
                                        <select style="margin-bottom: 10px;" name="gastring" class="data form-control" id="klinik">
                                            <option hidden>-- Please Select -- </option>
                                            <option value="Ada">Ada</option>
                                            <option value="Tidak Ada">Tidak Ada</option>
                                        </select>

                                        <label for="alamat">Penyakit Lain</label>
                                        <select style="margin-bottom: 10px;" name="lain" class="data form-control" id="klinik">
                                            <option hidden>-- Please Select -- </option>
                                            <option value="Ada">Ada</option>
                                            <option value="Tidak Ada">Tidak Ada</option>
                                        </select>

                                        <label for="tlp">Alergi Obat</label>
                                        <select style="margin-bottom: 10px;" name="a_obat" class="data form-control" id="klinik">
                                            <option hidden>-- Please Select -- </option>
                                            <option value="Ada">Ada</option>
                                            <option value="Tidak Ada">Tidak Ada</option>
                                        </select>

                                        <label for="tlp">Alergi Makanan</label>
                                        <select style="margin-bottom: 10px;" name="a_makan" class="data form-control" id="klinik">
                                            <option hidden>-- Please Select -- </option>
                                            <option value="Ada">Ada</option>
                                            <option value="Tidak Ada">Tidak Ada</option>
                                        </select>

                                       

                                     
                                </div>
                            </div>
                        </div>
                        <button class="myButton" type="submit" >submit</button>
                        </form> 
                    </div>  
                  </div>      
                </div>
                <!-- /#page-wrapper -->

            </div>

            <!-- /#wrapper -->


<!-- Modal -->
<div class="modal fade" id="fotomodal" role="dialog">
    <div class="modal-dialog">

      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Edit Foto</h4>
      </div>
      <div class="modal-body">
        <form method="post" action="<?php echo base_url(). 'index.php/perawat/foto'; ?>" enctype="multipart/form-data">
          <label>Masukkan foto</label> 
          <input style="border:none;" class="form-control data" type="file" id="lname" name="foto">
      
  </div>
  <div class="modal-footer">
      <button type="submit" class="btn btn-default" >save</button>
  </div>
</div>
</form>

</div>
</div>

<!-- /Modal-->



<!-- jQuery -->
<script src="<?php echo base_url("asset/js/jquery.js");?>"></script>

<!-- Bootstrap Core JavaScript -->
<script src="<?php echo base_url("asset/js/bootstrap.min.js");?>"></script>

<!-- Morris Charts JavaScript -->
<script src="<?php echo base_url("asset/js/plugins/morris/raphael.min.js");?>"></script>
<script src="<?php echo base_url("asset/js/plugins/morris/morris.min.js");?>"></script>
<script src="<?php echo base_url("asset/js/plugins/morris/morris-data.js");?>"></script>

</body>

</html>
