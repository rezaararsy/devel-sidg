<!DOCTYPE html>
<html>
<head>
	<title></title>
	<!-- Latest compiled and minified CSS -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

	<!-- Optional theme -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
	<link href="<?php echo base_url("asset/css/admin.css");?>" rel="stylesheet">
	<link rel='stylesheet prefetch' href='https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.2/css/font-awesome.min.css'>
	<link href="https://fonts.googleapis.com/css?family=Poppins" rel="stylesheet">
</head>
<body style="font-family: 'Poppins', sans-serif;">
	<div id="wrapper" style="">
        <!-- Sidebar -->
        <div id="sidebar-wrapper">
            <ul class="sidebar-nav" style="color:white">
                <li style="margin-top: 20px; margin-bottom: 20px;">SIDG</li>
                <li>
                <li>
                    <a href="<?php echo base_url(). 'index.php/admin/'; ?>"><i class="fa fa-feed" aria-hidden="true"  ></i> Dashboard</a>
                </li>
                <li>
                    <a href="<?php echo base_url(). 'index.php/pelayanan/'; ?>"></i><i class="fa fa-envelope" aria-hidden="true"  ></i> Pelayanan</a>
                </li>
                <li>
                    <a href="RequestAplikasi.html"></i><i class="fa fa-desktop" aria-hidden="true"  ></i>   Administrasi</a>
                </li>
            </ul>
        </div>
        <!-- /#sidebar-wrapper -->
        <!-- Page Content -->
        <div id="page-content-wrapper" style="padding: 0px;">
            <div class="container-fluid">
                <div class="row">
                  <div class="col-lg-12" style="background-color:#ecf0f1;">
                    <a href="#menu-toggle" id="menu-toggle"><i class="fa fa-bars fa-2x" aria-hidden="true"></i></a>
                    <ul class="nav navbar-nav navbar-right">
                      <li><a href="#" style="color: #2a3f54;"><span class="glyphicon glyphicon-log-in" style="color: #2a3f54;"></span> Logout</a></li>
                  </ul>
                  </div> 
                    <div class="col-lg-12">
                      <h1>FORM PEMERIKSAAN PASIEN</h1>
                      <section></section>
                      <h5>No Antrian        : </h5>
                      <h5>Nama              : </h5>
                      <h5>Alamat            : </h5>
                      <h5>Tgl Berkunjung    : </h5>
                      <h5>Nama Pemeriksa    : </h5>
                      <div class="dropdown">
                          <button class="btn btn-default dropdown-toggle" type="button" id="menu1" data-toggle="dropdown">Pemeriksa <span class="caret"></span></button>
                          <ul class="dropdown-menu" role="menu" aria-labelledby="menu1">
                            <li role="presentation"><a role="menuitem" tabindex="-1" href="#">HTML</a></li>
                            <li role="presentation"><a role="menuitem" tabindex="-1" href="#">CSS</a></li>
                            <li role="presentation"><a role="menuitem" tabindex="-1" href="#">JavaScript</a></li>
                          </ul>
                      </div>

                      <div style="margin-top: 20px;">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                      tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                      quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                      consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
                      cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
                      proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /#page-content-wrapper -->
        
    </div>

	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
	<!-- Latest compiled and minified JavaScript -->
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
	<script  src="<?php echo base_url("asset/js/index.js");?>"></script>
</body>
</html>