<!DOCTYPE html>
<html>
<head>
    <title></title>
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

    <!-- Optional theme -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
    <link href="<?php echo base_url("asset/css/admin.css");?>" rel="stylesheet">
    <link rel='stylesheet prefetch' href='https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.2/css/font-awesome.min.css'>
    <link href="https://fonts.googleapis.com/css?family=Poppins" rel="stylesheet">
</head>
<body style="font-family: 'Poppins', sans-serif;">
    <div id="wrapper" style="">
        <!-- Sidebar -->
        <div id="sidebar-wrapper">
            <ul class="sidebar-nav" style="color:white">
                <li style="margin-top: 20px; margin-bottom: 20px;">SIDG</li>
                <li>
                <li>
                    <a href="<?php echo base_url(). 'index.php/admin/'; ?>"><i class="fa fa-feed" aria-hidden="true"  ></i> Dashboard</a>
                </li>
                <li>
                    <a href="<?php echo base_url(). 'index.php/pelayanan/'; ?>"></i><i class="fa fa-envelope" aria-hidden="true"  ></i> Pelayanan</a>
                </li>
                <li>
                    <a href="RequestAplikasi.html"></i><i class="fa fa-desktop" aria-hidden="true"  ></i>   Administrasi</a>
                </li>
            </ul>
        </div>
        <!-- /#sidebar-wrapper -->
        <!-- Page Content -->
        <div id="page-content-wrapper" style="padding: 0px;">
            <div class="container-fluid">
                <div class="row">
                  <div class="col-lg-12" style="background-color:#ecf0f1;">
                    <a href="#menu-toggle" id="menu-toggle"><i class="fa fa-bars fa-2x" aria-hidden="true"></i></a>
                    <ul class="nav navbar-nav navbar-right">
                      <li><a href="#" style="color: #2a3f54;"><span class="glyphicon glyphicon-log-in" style="color: #2a3f54;"></span> Logout</a></li>
                  </ul>
                  </div> 
                    <div class="col-lg-12">
                      <h1>ANTRIAN PELAYANAN HARI INI</h1>
                      <div class="">
                        <table class="table table-responsive">
                          <thead>
                            <th>AKSI</th>
                            <th>No. Antrian</th>
                            <th>Nama</th>
                            <th>Alamat</th>
                            <th>Tanggal Berkunjung</th>
                          </thead>
                          <tbody>
                            <tr>
                              <td>
                                <a href="<?php echo base_url(). 'index.php/pelayanan/layani'; ?>">
                                <button type="submit" class="btn btn-info pull-center"><i class="fa fa-fw fa-sign-in"></i>LAYANI</button></a>
                              </td>
                              <td>1</td>
                              <td>Reza</td>
                              <td>Joyogrand</td>
                              <td>10 Mei 2018</td>
                            </tr>
                            <tr>
                              <td>
                                <button type="submit" class="btn btn-info pull-center"><i class="fa fa-fw fa-sign-in"></i>LAYANI</button>
                              </td>
                              <td>1</td>
                              <td>Reza</td>
                              <td>Joyogrand</td>
                              <td>10 Mei 2018</td>
                            </tr>
                            <tr>
                              <td>
                                <button type="submit" class="btn btn-info pull-center"><i class="fa fa-fw fa-sign-in"></i>LAYANI</button>
                              </td>
                              <td>1</td>
                              <td>Reza</td>
                              <td>Joyogrand</td>
                              <td>10 Mei 2018</td>
                            </tr>
                          </tbody>
                        </table>
                      </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /#page-content-wrapper -->
        
    </div>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <!-- Latest compiled and minified JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
    <script  src="<?php echo base_url("asset/js/index.js");?>"></script>
</body>
</html>