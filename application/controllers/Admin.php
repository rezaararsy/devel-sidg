<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {

	function __construct()
	  {
	    parent::__construct();
	    $this->load->model('m_sidg');
	    $this->load->helper('url');
	    if($this->session->userdata('status') != "login"){
		   redirect('');
		  }
		 
	  }

	public function index()
	{
		$klinik = $this->session->userdata('klinik');
		$data['hasil'] = $this->m_sidg->getDaftarPasienLayanan($klinik);
		$this->load->view('admin/v_layanan_pasien',$data);
	}

/*	public function layanan_pasien()
	{	
		$klinik = $this->session->userdata('klinik');
		$data['hasil'] = $this->m_sidg->getDaftarPasienLayanan($klinik);
		$this->load->view('v_layanan_pasien',$data);
	}*/

	public function profile()
	{
		$this->load->view('admin/v_profile');
	}

	public function tabel_pasien()
	{
		$klinik = $this->session->userdata('klinik');
		$data['hasil'] = $this->m_sidg->getDaftarPasien2($klinik);
		$this->load->view('admin/v_tabel_pasien',$data);
	}

	public function tabel_perawatan()
	{
		$this->load->view('admin/v_tabel_perawatan');
	}

	public function laporan()
	{
		$this->load->view('admin/v_laporan');
	}
	public function ondogram()
	{	
		$data['hasil'] = $this->m_sidg->getTindakan();
		$id = $this->uri->segment(3);
		$this->session->set_userdata('id', $id);
		$this->load->view('admin/v_ondogram',$data);
	}
	

	public function pegawai()
	{
	    $data['hasil'] = $this->m_sidg->getDaftarPegawai();
		$this->load->view('admin/v_pegawai',$data);
	}
	public function tindakan()
	{	
		$data['hasil'] = $this->m_sidg->getTindakan();
		$this->load->view('admin/v_tindakan',$data);
	}

	public function tambah_tindakan()
	{
		$tindakan = $this->input->post('tindakan');
		$harga = $this->input->post('harga');

		$data = array(
		'tindakan' => $tindakan,
		'harga' => $harga
		);

		$this->m_sidg->input_data($data,'tindakan');

		$this->session->set_flashdata('message', 'Anda berhasil menginput data');
		redirect(base_url("index.php/admin/tindakan"));
	}

	public function foto(){
		$config['upload_path']          = './asset/images/profil/admin/';
		$config['allowed_types']        = 'gif|jpg|png';
		$config['file_name']       	 	= $this->session->userdata('nama');
		$config['max_size']             = 2000;


	 
		$this->load->library('upload', $config);
	 
		if ( ! $this->upload->do_upload('foto')){
			$error = array('error' => $this->upload->display_errors());
/*			$this->load->view('v_upload', $error);*/
			echo $error;
		}else{
			$data = array('upload_data' => $this->upload->data());
			/*$this->load->view('v_upload_sukses', $data);*/
			$nama = $this->upload->data('file_name'); 
			$linke = '/asset/images/profil/admin/'.$nama;
			$id = $this->session->userdata('nama');
			$this->m_sidg->update_fotoA($id,$linke);
			$this->session->set_userdata('foto', $linke);
		}	
		
	}

	public function tambahAkun()
	{
		$username = $this->input->post('username');
		$password = $this->input->post('password');
		$role = $this->input->post('role');

		$data = array(
		'username' => $username,
		'password' => md5($password),
		'role' => $role
		);

		$this->m_sidg->input_daftarAkun($data,'admin');

		$this->session->set_flashdata('message', 'Anda berhasil menginput data');
		redirect(base_url("index.php/admin/pegawai"));

	}

	public function inputgigiFinish(){
		$occlusi = $this->input->post('occlusi');
		$torusP = $this->input->post('torusP');
		$torusM = $this->input->post('torusM');
		$palatum = $this->input->post('palatum');
		$diastema = $this->input->post('diastema');
		$gigi = $this->input->post('gigi');

		$id = $this->session->userdata('id');
		$tgl=date('d');
		$idpr = "$id$tgl";

		$data = array(
		'id_perawatan' => $idpr,
		'id_pasien' => $id,
		'occlusi' => $occlusi,
		'torus_pala' => $torusP,
		'torus_mandi' => $torusM,
		'palatum' => $palatum,
		'diastema' => $diastema,
		'gigi_anomali' => $gigi
		);

		$set = 1;
		
		$this->m_sidg->tutup_layanann($set,$id);
		$this->m_sidg->input_data($data,'periksa');
		
		redirect(base_url("index.php/admin/"));

	}

	public function inputgigi($kode){
		$penyakitM = $this->input->post('penyakitM');
		$restorasiM = $this->input->post('restorasiM');
		$protesaM = $this->input->post('protesaM');
		$icdM = $this->input->post('icdm');
		$tindakanM = $this->input->post('tindakanm');
		$penyakitO = $this->input->post('penyakitO');
		$restorasiO = $this->input->post('restorasiO');
		$protesaO = $this->input->post('protesaO');
		$icdO = $this->input->post('icdo');
		$tindakanO = $this->input->post('tindakano');
		$penyakitD = $this->input->post('penyakitD');
		$restorasiD = $this->input->post('restorasiD');
		$protesaD = $this->input->post('protesaD');
		$icdD = $this->input->post('icdd');
		$tindakanD = $this->input->post('tindakand');
		$penyakitV = $this->input->post('penyakitV');
		$restorasiV = $this->input->post('restorasiV');
		$protesaV = $this->input->post('protesaV');
		$icdV = $this->input->post('icdv');
		$tindakanV = $this->input->post('tindakanv');
		$penyakitL = $this->input->post('penyakitL');
		$restorasiL = $this->input->post('restorasiL');
		$protesaL = $this->input->post('protesaL');
		$icdL = $this->input->post('icdl');
		$tindakanL = $this->input->post('tindakanl');

		$id = $this->session->userdata('id');
		$id_dokter = $this->session->userdata('idku');
		$tgl=date('d');
		$idpr = "$id$tgl";

		$M = "$kode-M";
		$O = "$kode-O";
		$D = "$kode-D";
		$V = "$kode-V";
		$L = "$kode-L";

		$dataM = array(
		'id_perawatan' => $idpr,	
		'id_pasien' => $id,
		'id_gigi' => $M,
		'id_dokter' => $id_dokter,
		'penyakit' => $penyakitM,
		'restorasi' => $restorasiM,
		'protesa' => $protesaM,
		'icd' => $icdM,
		'tindakan' => $tindakanM
		);
		
		$this->m_sidg->input_data($dataM,'gigi');


		$dataO = array(
		'id_perawatan' => $idpr,	
		'id_pasien' => $id,
		'id_gigi' => $O,
		'id_dokter' => $id_dokter,
		'penyakit' => $penyakitO,
		'restorasi' => $restorasiO,
		'protesa' => $protesaO,
		'icd' => $icdO,
		'tindakan' => $tindakanO
		);
		
		$this->m_sidg->input_data($dataO,'gigi');

		$dataD = array(
		'id_perawatan' => $idpr,
		'id_pasien' => $id,
		'id_gigi' => $D,
		'id_dokter' => $id_dokter,
		'penyakit' => $penyakitD,
		'restorasi' => $restorasiD,
		'protesa' => $protesaD,
		'icd' => $icdD,
		'tindakan' => $tindakanD
		);
		
		$this->m_sidg->input_data($dataD,'gigi');

		$dataV = array(
		'id_perawatan' => $idpr,
		'id_pasien' => $id,
		'id_gigi' => $V,
		'id_dokter' => $id_dokter,
		'penyakit' => $penyakitV,
		'restorasi' => $restorasiV,
		'protesa' => $protesaV,
		'icd' => $icdV,
		'tindakan' => $tindakanV
		);
		
		$this->m_sidg->input_data($dataV,'gigi');

		$dataL = array(
		'id_perawatan' => $idpr,
		'id_pasien' => $id,
		'id_gigi' => $L,
		'id_dokter' => $id_dokter,
		'penyakit' => $penyakitL,
		'restorasi' => $restorasiL,
		'protesa' => $protesaL,
		'icd' => $icdL,
		'tindakan' => $tindakanL
		);
		
		$this->m_sidg->input_data($dataL,'gigi');
		
		redirect(base_url("index.php/admin/ondogram/$id"));
	}
}
