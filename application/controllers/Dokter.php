<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dokter extends CI_Controller {

	
	function __construct()
	  {
	    parent::__construct();
	    $this->load->model('m_sidg');
	    $this->load->helper('url');
	    if($this->session->userdata('status') != "login"){
		   redirect('');
		  }
		 
	  }
	public function index()
	{	
		$klinik = $this->session->userdata('klinik');
		$data['hasil'] = $this->m_sidg->getDaftarPasienLayanan($klinik);
		$this->load->view('dokter/v_layan_dokter',$data);
	}
	public function laporan()
	{
		$this->load->view('dokter/v_laporan_dokter');
	}
	public function foto(){
		$config['upload_path']          = './asset/images/profil/dokter/';
		$config['allowed_types']        = 'gif|jpg|png';
		$config['file_name']       	 	= $this->session->userdata('nama');
		$config['max_size']             = 2000;


	 
		$this->load->library('upload', $config);
	 
		if ( ! $this->upload->do_upload('foto')){
			$error = array('error' => $this->upload->display_errors());
/*			$this->load->view('v_upload', $error);*/
			echo $error;
		}else{
			$data = array('upload_data' => $this->upload->data());
			/*$this->load->view('v_upload_sukses', $data);*/
			$nama = $this->upload->data('file_name'); 
			$linke = '/asset/images/profil/dokter/'.$nama;
			$id = $this->session->userdata('nama');
			$this->m_sidg->update_fotoA($id,$linke);
			$this->session->set_userdata('foto', $linke);
		}		
	}
}
