<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pasien extends CI_Controller {

		function __construct()
	  {
	    parent::__construct();
	    $this->load->model('m_sidg');
	    $this->load->helper(array('form', 'url'));
	    if($this->session->userdata('status') != "login"){
		   redirect('');
		  }
		 
	  }
	public function index()
	{	
		$klinik = $this->session->userdata('klinik');
		$data['dokter'] = $this->m_sidg->getDataDokter($klinik);
		$data['klinik'] = $this->m_sidg->getDataKlinik($klinik);
		$this->load->view('pasien/v_pasien',$data);
	}

	public function foto(){
		$config['upload_path']          = './asset/images/profil/pasien/';
		$config['allowed_types']        = 'gif|jpg|png';
		$config['file_name']       	 	= $this->session->userdata('username');
		$config['max_size']             = 2000;


	 
		$this->load->library('upload', $config);
	 
		if ( ! $this->upload->do_upload('foto')){
			$error = array('error' => $this->upload->display_errors());
/*			$this->load->view('v_upload', $error);*/
			echo $error;
		}else{
			$data = array('upload_data' => $this->upload->data());
			/*$this->load->view('v_upload_sukses', $data);*/
			$nama = $this->upload->data('file_name'); 
			$linke = '/asset/images/profil/pasien/'.$nama;
			$id = $this->session->userdata('username');
			$this->m_sidg->update_foto($id,$linke);
			$this->session->set_userdata('foto', $linke);
		}	
		
	}


	public function pesan(){
		$klinik = $this->session->userdata('klinik');
		$id = $this->session->userdata('id');
		$ambil = $this->m_sidg->getDataKlinikRow($klinik);
		if (isset($ambil)) {
			$antrianya = $ambil->antrian;
		}
		$antrianya = $antrianya + 1;
		$this->m_sidg->i_antrian_pasien($antrianya,$id);
		$this->m_sidg->i_antrian_klinik($antrianya,$klinik);
		$this->session->set_userdata('antrian',$antrianya);
		redirect(base_url("index.php/pasien/antrian"));
	}
	public function antrian(){
		$klinik = $this->session->userdata('klinik');
		$data['antri'] = $this->m_sidg->getDataKlinik($klinik);
		$this->load->view('pasien/v_antrian',$data);
	}

	public function riwayat()
	{	
		$this->load->view('pasien/v_riwayat');
	}

	public function bantuan()
	{	
		$this->load->view('pasien/v_bantuan');
	}
}
