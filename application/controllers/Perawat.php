<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Perawat extends CI_Controller {

	
	function __construct()
	  {
	    parent::__construct();
	    $this->load->model('m_sidg');
	    $this->load->helper('url');
	    if($this->session->userdata('status') != "login"){
		   redirect('');
		  }
		 
	  }
	public function index()
	{
		$this->load->view('perawat/v_perawat_daftar');
	}
	public function antrian()
	{	
		$klinik = $this->session->userdata('klinik');
		$data['hasil'] = $this->m_sidg->getDaftarPasienLayanan($klinik);
		$this->load->view('perawat/v_antrian_perawat',$data);
	}
	public function tabel_pasien()
	{	
		$klinik = $this->session->userdata('klinik');
		$data['hasil'] = $this->m_sidg->getDaftarPasien2($klinik);
		$this->load->view('perawat/v_tabel_pasien_perawat',$data);
	}
	public function buka()
	{	
		$antrian = 1;
		$klinik = $this->session->userdata('klinik');
		$this->m_sidg->antrian($antrian,$klinik);
		redirect(base_url("index.php/perawat/antrian"));	
	}
	public function tutup()
	{	
		$antrian = NULL;
		$klinik = $this->session->userdata('klinik');
		echo $klinik;
		$this->m_sidg->antrian($antrian,$klinik);
		redirect(base_url("index.php/perawat/antrian"));	
	}
	public function inputData()
	{
		$username = $this->input->post('username');
		$password = $this->input->post('password');
		$nama = $this->input->post('nama');
		$no_ktp = $this->input->post('no_ktp');
		$usia = $this->input->post('usia');
		$jk = $this->input->post('jk');
		$tmpl = $this->input->post('tmpl');
		$tgll = $this->input->post('tgll');
		$suku = $this->input->post('suku');
		$pekerjaan = $this->input->post('pekerjaan');
		$alamat = $this->input->post('alamat');
		$no_hp = $this->input->post('no_hp');
		$klinik = $this->session->userdata('klinik');


		$gol_darah = $this->input->post('gol_darah');
		$tkn_darah = $this->input->post('tkn_darah');
		$pjantung= $this->input->post('pjantung');
		$diabetes= $this->input->post('diabetes');
		$haemo= $this->input->post('haemo');
		$hepa= $this->input->post('hepa');
		$gastring= $this->input->post('gastring');
		$lain= $this->input->post('lain');
		$a_obat= $this->input->post('a_obat');
		$a_makan= $this->input->post('a_makan');

		$data = array(
		'username' => $username,
		'password' => md5($password),
		'nama' => $nama,
		'nik' => $no_ktp,
		'usia' => $usia,
		'jk' => $jk,
		'tmpl' => $tmpl,
		'tgll' => $tgll,
		'suku' => $suku,
		'pekerjaan' => $pekerjaan,
		'alamat' => $alamat,
		'telp' => $no_hp,
		'klinik' => $klinik
		);

		$data2 = array(
		'gol_darah' => $gol_darah,
		'tekanan_darah' => $tkn_darah,
		'penyakit_jantung' => $pjantung,
		'diabetes' => $diabetes,
		'haemopilia' => $haemo,
		'hepatitis' => $hepa,
		'gastring' => $gastring,
		'lainya' => $lain,
		'alergi_obat' => $a_obat,
		'alergi_makanan' => $a_makan
		);

		$this->m_sidg->input_daftar($data,'pasien');
		$this->m_sidg->input_daftar($data2,'data_medis');

		$this->session->set_flashdata('message', 'Anda berhasil menginput data');
		redirect(base_url("index.php/perawat"));
	}
	public function foto(){
		$config['upload_path']          = './asset/images/profil/perawat/';
		$config['allowed_types']        = 'gif|jpg|png';
		$config['file_name']       	 	= $this->session->userdata('nama');
		$config['max_size']             = 2000;


	 
		$this->load->library('upload', $config);
	 
		if ( ! $this->upload->do_upload('foto')){
			$error = array('error' => $this->upload->display_errors());
/*			$this->load->view('v_upload', $error);*/
			echo $error;
		}else{
			$data = array('upload_data' => $this->upload->data());
			/*$this->load->view('v_upload_sukses', $data);*/
			$nama = $this->upload->data('file_name'); 
			$linke = '/asset/images/profil/perawat/'.$nama;
			$id = $this->session->userdata('nama');
			$this->m_sidg->update_fotoA($id,$linke);
			$this->session->set_userdata('foto', $linke);
		}		
	}
}
