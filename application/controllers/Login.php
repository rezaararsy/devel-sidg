<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {
	function __construct()
	  {
	    parent::__construct();
	    $this->load->model('M_sidg', 'Model');
	  }
	public function index()
	{	
		$username = $this->input->post('username');
		$password = $this->input->post('password');
		$where = array(
			'username' => $username,
			'password' => md5($password)
			);
		$cek = $this->Model->cek_login("pasien",$where)->num_rows();
		if($cek > 0){
			$ambil = $this->Model->cek_login("pasien",$where);
			$data_session = array(
				'nama' => $ambil->row()->nama,
				'username' => $ambil->row()->username,
				'foto' => $ambil->row()->foto,
				'klinik' => $ambil->row()->klinik,
				'id' => $ambil->row()->id,
				'antrian' => $ambil->row()->antrian,
				'status' => "login",
				);
 
			$this->session->set_userdata($data_session);
			redirect(base_url("index.php/pasien"));
 
		}else{
			$cek2 = $this->Model->cek_login("admin",$where)->num_rows();
			if($cek2 > 0){
			$ambile = $this->Model->cek_login("admin",$where);
			$data_session = array(
				'nama' => $ambile->row()->username,
				'foto' => $ambile->row()->foto,
				'role' => $ambile->row()->role,
				'klinik' => $ambile->row()->klinik,
				'idku' => $ambile->row()->id,
				'status' => "login"
				);
			$this->session->set_userdata($data_session);
			$role = $this->session->userdata('role');
			if ($role == 1) {
			 	redirect(base_url("index.php/admin"));
			 }elseif ($role == 2) {
			 	redirect(base_url("index.php/dokter"));
			 }elseif ($role == 3) {
			 	redirect(base_url("index.php/perawat"));
			 }elseif ($role == 4) {
			 	redirect(base_url("index.php/kasir"));
			 }
			
			}else{
				$cek3 = $this->Model->cek_login("super",$where)->num_rows();
				if ($cek3 > 0) {
					redirect(base_url("index.php/login/super"));
				}else{
					$this->session->set_flashdata('message', 'Username atau Paswword Salah');
				redirect("");
				}
				
			}
		}
	}
	function logout(){
		$this->session->sess_destroy();
  	redirect('');
	}
	function daftar(){
		$this->load->view('v_pendaftaran');
	}
	function super(){
		$this->load->view('v_klinik');
	}
	function daftar_klinik(){
		$klinik = $this->input->post('klinik');
		$nama = $this->input->post('nama');
		$username = $this->input->post('username');
		$password = $this->input->post('password');
		$role =1;

		$data = array(
		'nama_klinik' => $klinik
		);

		$data2 = array(
		'nama' => $nama,
		'username' => $username,
		'password' => $password,
		'role' => $role
		);
		
		$this->Model->input_daftar($data,'klinik');
		$this->Model->input_daftar($data2,'admin');

		$this->session->set_flashdata('message', 'Anda berhasil menginput data');
		redirect(base_url("index.php/login/super"));
	}
}
