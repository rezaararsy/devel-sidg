<?php

defined('BASEPATH') OR exit ('No direct script access allowed');

/**
 * Model for ICD-10 Web Application
 */
class M_sidg extends CI_Model
{

  function __construct()
  {
    parent::__construct();
  }
  function cek_login($table,$where){    
    return $this->db->get_where($table,$where);
  }
  function input_daftar($data,$table){
    $this->db->insert($table,$data);
  }
  function input_data($data,$table){
    $this->db->insert($table,$data);
  }
  function input_daftarAkun($data,$table){
    $this->db->insert($table,$data);
  }
  function getDaftarPegawai(){
    $query=$this->db->query("SELECT username, role, foto FROM admin ORDER BY id ASC");
    return $query->result();
  }
  function getTindakan(){
    $query=$this->db->query("SELECT id, tindakan, harga FROM tindakan ORDER BY id ASC");
    return $query->result();
  }
  function getDaftarPasien(){
    $query=$this->db->query("SELECT nama, nik, usia, jk, tgll, suku, pekerjaan, alamat, telp FROM pasien");
    return $query->result();
  }
  function getDaftarPasien2($klinik){
    $query=$this->db->query("SELECT id,nama, nik, usia, jk, tgll, suku, pekerjaan, alamat, telp, klinik, antrian FROM pasien WHERE klinik = '$klinik' ORDER BY id ASC");
    return $query->result();
  }
  function getDaftarPasienLayanan($klinik){
    $query=$this->db->query("SELECT id,nama, nik, usia, jk, tgll, suku, pekerjaan, alamat, telp, klinik, antrian FROM pasien WHERE klinik = '$klinik' AND antrian IS NOT NULL ORDER BY antrian ASC");
    return $query->result();
  }
  function getDataDokter($klinik){
    $query=$this->db->query("SELECT nama FROM admin WHERE klinik = '$klinik'");
    return $query->result();
  }
  function getDataKlinik($klinik){
    $query=$this->db->query("SELECT nama_klinik,buka, antrian, layan FROM klinik WHERE id_klinik = '$klinik'");
    return $query->result();
  }
  function getDataKlinikRow($klinik){
    $query=$this->db->query("SELECT nama_klinik,buka, antrian FROM klinik WHERE id_klinik = '$klinik'");
    return $query->row();
  }

  function update_foto($id,$link){
    $this->db->set('foto', $link);
    $this->db->where('username', $id);
    $this->db->update('pasien');
  }
  function i_antrian_pasien($antri,$pasien){
    $this->db->set('antrian', $antri);
    $this->db->where('id', $pasien);
    $this->db->update('pasien');
  }
  function i_antrian_klinik($antri,$klinik){
    $this->db->set('antrian', $antri);
    $this->db->where('id_klinik', $klinik);
    $this->db->update('klinik');
  }
  function update_fotoA($id,$link){
    $this->db->set('foto', $link);
    $this->db->where('username', $id);
    $this->db->update('admin');
  }
  function antrian($antrian,$klinik){
    $this->db->set('buka', $antrian);
    $this->db->where('id_klinik', $klinik);
    $this->db->update('klinik');
  }
  function tutup_layanann($set,$id_pasien){
    $this->db->set('antrian', $set);
    $this->db->where('id', $id_pasien);
    $this->db->update('pasien');
  }
}
