-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: 03 Sep 2018 pada 13.43
-- Versi Server: 10.1.30-MariaDB
-- PHP Version: 5.6.33

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `sidg`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `admin`
--

CREATE TABLE `admin` (
  `id` int(11) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `role` int(11) NOT NULL,
  `username` varchar(200) NOT NULL,
  `foto` varchar(500) NOT NULL,
  `password` varchar(200) NOT NULL,
  `klinik` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `admin`
--

INSERT INTO `admin` (`id`, `nama`, `role`, `username`, `foto`, `password`, `klinik`) VALUES
(1, 'Dr. Reza', 1, 'reza', '/asset/images/profil/admin/reza.jpg', '819d6bcea1f9c6864abdcfe1646e2adc', 1),
(2, 'Dr. Admin', 1, 'admin', '/asset/images/profil/admin/admin.jpg', '21232f297a57a5a743894a0e4a801fc3', 1),
(3, '', 3, 'rawat', '/asset/images/profil/perawat/rawat.jpg', '22e75c49b7269540c8e8a1174cd5d34f', 1),
(4, '', 2, 'dokter', '', 'd22af4180eee4bd95072eb90f94930e5', 1),
(5, '', 4, 'kasir', '', 'c7911af3adbd12a035b289556d96470a', 1),
(6, 'fajar', 1, 'jarwo', '', 'jarwo', 0);

-- --------------------------------------------------------

--
-- Struktur dari tabel `data_medis`
--

CREATE TABLE `data_medis` (
  `id_pasien` int(11) NOT NULL,
  `gol_darah` varchar(20) NOT NULL,
  `alergi_makanan` varchar(100) NOT NULL,
  `tekanan_darah` varchar(100) NOT NULL,
  `alergi_obat` varchar(100) NOT NULL,
  `penyakit_jantung` varchar(100) NOT NULL,
  `diabetes` varchar(100) NOT NULL,
  `haemopilia` varchar(100) NOT NULL,
  `hepatitis` varchar(100) NOT NULL,
  `gastring` varchar(100) NOT NULL,
  `lainya` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `data_medis`
--

INSERT INTO `data_medis` (`id_pasien`, `gol_darah`, `alergi_makanan`, `tekanan_darah`, `alergi_obat`, `penyakit_jantung`, `diabetes`, `haemopilia`, `hepatitis`, `gastring`, `lainya`) VALUES
(1, 'A', 'Tidak Ada', '100', 'Tidak Ada', 'Tidak Ada', 'Tidak Ada', 'Tidak Ada', 'Ada', 'Tidak Ada', 'Tidak Ada'),
(2, '-- Please Select --', '-- Please Select --', '', '-- Please Select --', '-- Please Select --', '-- Please Select --', '-- Please Select --', '-- Please Select --', '-- Please Select --', '-- Please Select --'),
(3, '-- Please Select --', '-- Please Select --', '', '-- Please Select --', '-- Please Select --', '-- Please Select --', '-- Please Select --', '-- Please Select --', '-- Please Select --', '-- Please Select --');

-- --------------------------------------------------------

--
-- Struktur dari tabel `gigi`
--

CREATE TABLE `gigi` (
  `id_perawatan` varchar(200) NOT NULL,
  `id_pasien` int(11) NOT NULL,
  `id_gigi` int(11) NOT NULL,
  `id_dokter` int(11) NOT NULL,
  `penyakit` varchar(100) NOT NULL,
  `restorasi` varchar(100) NOT NULL,
  `protesa` varchar(100) NOT NULL,
  `icd` varchar(200) NOT NULL,
  `tindakan` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `gigi`
--

INSERT INTO `gigi` (`id_perawatan`, `id_pasien`, `id_gigi`, `id_dokter`, `penyakit`, `restorasi`, `protesa`, `icd`, `tindakan`) VALUES
('0.31', 2, 18, 0, 'Tidak Ada', 'Tidak Ada', 'Tidak Ada', '', ''),
('0.31', 2, 18, 0, 'Tidak Ada', 'Tidak Ada', 'Tidak Ada', '', ''),
('0.31', 2, 18, 0, 'Tidak Ada', 'Tidak Ada', 'Tidak Ada', '', ''),
('0.31', 2, 18, 0, 'Tidak Ada', 'Tidak Ada', 'Tidak Ada', '', ''),
('0.31', 2, 18, 0, 'Tidak Ada', 'Tidak Ada', 'Tidak Ada', '', ''),
('.2..11.', 2, 82, 0, 'Tidak Ada', 'Tidak Ada', 'Tidak Ada', '', ''),
('.2..11.', 2, 82, 0, 'Tidak Ada', 'Tidak Ada', 'Tidak Ada', '', ''),
('.2..11.', 2, 82, 0, 'Tidak Ada', 'Tidak Ada', 'Tidak Ada', '', ''),
('.2..11.', 2, 82, 0, 'Tidak Ada', 'Tidak Ada', 'Tidak Ada', '', ''),
('.2..11.', 2, 82, 0, 'Tidak Ada', 'Tidak Ada', 'Tidak Ada', '', ''),
('.211.', 2, 83, 0, 'Tidak Ada', 'Tidak Ada', 'Tidak Ada', '', ''),
('.211.', 2, 83, 0, 'Tidak Ada', 'Tidak Ada', 'Tidak Ada', '', ''),
('.211.', 2, 83, 0, 'Tidak Ada', 'Tidak Ada', 'Tidak Ada', '', ''),
('.211.', 2, 83, 0, 'Tidak Ada', 'Tidak Ada', 'Tidak Ada', '', ''),
('.211.', 2, 83, 0, 'Tidak Ada', 'Tidak Ada', 'Tidak Ada', '', ''),
('211', 2, 31, 0, 'Tidak Ada', 'Tidak Ada', 'Tidak Ada', '', ''),
('211', 2, 31, 0, 'Tidak Ada', 'Tidak Ada', 'Tidak Ada', '', ''),
('211', 2, 31, 0, 'Tidak Ada', 'Tidak Ada', 'Tidak Ada', '', ''),
('211', 2, 31, 0, 'Tidak Ada', 'Tidak Ada', 'Tidak Ada', '', ''),
('211', 2, 31, 0, 'Tidak Ada', 'Tidak Ada', 'Tidak Ada', '', '');

-- --------------------------------------------------------

--
-- Struktur dari tabel `klinik`
--

CREATE TABLE `klinik` (
  `id_klinik` int(11) NOT NULL,
  `nama_klinik` varchar(100) NOT NULL,
  `antrian` int(11) DEFAULT NULL,
  `buka` int(11) DEFAULT NULL,
  `layan` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `klinik`
--

INSERT INTO `klinik` (`id_klinik`, `nama_klinik`, `antrian`, `buka`, `layan`) VALUES
(1, 'Dinoyo', 1, 1, NULL),
(2, 'Joyo', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `pasien`
--

CREATE TABLE `pasien` (
  `id` int(11) NOT NULL,
  `nama` varchar(200) NOT NULL,
  `telp` int(11) NOT NULL,
  `username` varchar(200) NOT NULL,
  `nik` int(20) NOT NULL,
  `password` varchar(200) NOT NULL,
  `jk` varchar(20) NOT NULL,
  `usia` int(11) NOT NULL,
  `tmpl` varchar(200) NOT NULL,
  `tgll` date NOT NULL,
  `tgl_pencatatan` date NOT NULL,
  `suku` varchar(20) NOT NULL,
  `pekerjaan` varchar(50) NOT NULL,
  `alamat` varchar(200) NOT NULL,
  `telp_seluler` int(11) NOT NULL,
  `klinik` int(11) NOT NULL,
  `antrian` int(11) DEFAULT NULL,
  `dokter` int(11) NOT NULL,
  `foto` varchar(300) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `pasien`
--

INSERT INTO `pasien` (`id`, `nama`, `telp`, `username`, `nik`, `password`, `jk`, `usia`, `tmpl`, `tgll`, `tgl_pencatatan`, `suku`, `pekerjaan`, `alamat`, `telp_seluler`, `klinik`, `antrian`, `dokter`, `foto`) VALUES
(1, 'Reza Ararsy', 2147483647, 'rezaararsy', 12344, '819d6bcea1f9c6864abdcfe1646e2adc', 'Ada', 21, 'Malang', '1997-05-10', '0000-00-00', 'Jawa', 'Mahasiswa', 'Malang', 0, 1, NULL, 0, '/asset/images/profil/pasien/rezaararsy.jpg'),
(2, 'Pasien', 0, 'pasien', 0, 'f5c25a0082eb0748faedca1ecdcfb131', '-- Please Select --', 0, '', '0000-00-00', '0000-00-00', '', '', '', 0, 1, 1, 0, '/asset/images/profil/pasien/pasien.jpg'),
(3, 'Fajar', 0, 'fajar', 0, '24bc50d85ad8fa9cda686145cf1f8aca', '-- Please Select --', 0, '', '2018-08-08', '0000-00-00', '', '', '', 0, 0, NULL, 0, '');

-- --------------------------------------------------------

--
-- Struktur dari tabel `perawatan`
--

CREATE TABLE `perawatan` (
  `id_pasien` int(11) NOT NULL,
  `tgl` date NOT NULL,
  `diagnosa` varchar(100) NOT NULL,
  `gigi_rawat` varchar(100) NOT NULL,
  `kode_icd` varchar(100) NOT NULL,
  `perawat` varchar(100) NOT NULL,
  `keterangan` varchar(150) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `periksa`
--

CREATE TABLE `periksa` (
  `id_perawatan` varchar(100) NOT NULL,
  `id_pasien` varchar(100) NOT NULL,
  `occlusi` varchar(100) NOT NULL,
  `torus_pala` varchar(100) NOT NULL,
  `torus_mandi` varchar(100) NOT NULL,
  `palatum` varchar(100) NOT NULL,
  `diastema` varchar(100) NOT NULL,
  `gigi_anomali` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `super`
--

CREATE TABLE `super` (
  `username` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `super`
--

INSERT INTO `super` (`username`, `password`) VALUES
('sasindo', '9c9bf059aa218140260faf019875f767');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tindakan`
--

CREATE TABLE `tindakan` (
  `id` int(11) NOT NULL,
  `tindakan` varchar(200) NOT NULL,
  `harga` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tindakan`
--

INSERT INTO `tindakan` (`id`, `tindakan`, `harga`) VALUES
(1, 'buntu', 5000),
(2, 'sakit', 2000),
(3, 'panas', 20000);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `data_medis`
--
ALTER TABLE `data_medis`
  ADD PRIMARY KEY (`id_pasien`);

--
-- Indexes for table `klinik`
--
ALTER TABLE `klinik`
  ADD PRIMARY KEY (`id_klinik`),
  ADD UNIQUE KEY `id_klinik` (`id_klinik`);

--
-- Indexes for table `pasien`
--
ALTER TABLE `pasien`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `antrian` (`antrian`);

--
-- Indexes for table `tindakan`
--
ALTER TABLE `tindakan`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `data_medis`
--
ALTER TABLE `data_medis`
  MODIFY `id_pasien` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `klinik`
--
ALTER TABLE `klinik`
  MODIFY `id_klinik` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `pasien`
--
ALTER TABLE `pasien`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `tindakan`
--
ALTER TABLE `tindakan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
